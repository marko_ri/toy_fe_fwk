
Toy frontend web framework in Webassembly, based on the book: [Build a Frontend Web Framework From Scratch](https://github.com/angelsolaorbaiceta/fe-fwk-book)


```
rustup target add wasm32-unknown-unknown
cargo install wasm-pack
wasm-pack build --target web --out-dir html/ch12/pkg
python3 -m http.server 8080 -d html
```
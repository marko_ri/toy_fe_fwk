use crate::{AfterHandler, CommandHandler};
use std::{cell::RefCell, collections::HashMap, rc::Rc};

#[derive(Debug, Clone, PartialEq)]
pub enum Payload {
    String(String),
    Usize(usize),
    Map(HashMap<String, Payload>),
    Empty,
    Test(Rc<RefCell<i32>>),
}

impl Payload {
    pub fn string(self) -> String {
        match self {
            Self::String(val) => val,
            _ => unreachable!(),
        }
    }

    pub fn usize(self) -> usize {
        match self {
            Self::Usize(val) => val,
            _ => unreachable!(),
        }
    }

    pub fn map(self) -> HashMap<String, Self> {
        match self {
            Self::Map(val) => val,
            _ => unreachable!(),
        }
    }

    pub fn test(self) -> Rc<RefCell<i32>> {
        match self {
            Self::Test(val) => val,
            _ => unreachable!(),
        }
    }
}

#[derive(Default)]
pub struct HandlerRegistry {
    command_handlers: HashMap<String, CommandHandler>,
    after_handlers: HashMap<String, AfterHandler>,
}

impl HandlerRegistry {
    pub fn subscribe(&mut self, command: &str, handler: CommandHandler) {
        if self.command_handlers.contains_key(command) {
            panic!("CommandHandler key taken: {}", command);
        }
        self.command_handlers.insert(command.to_string(), handler);
    }

    pub fn unsubscribe(&mut self, command: &str) {
        self.command_handlers.remove(command);
    }

    pub fn add_after_handler(&mut self, command: &str, handler: AfterHandler) {
        self.after_handlers.insert(command.to_string(), handler);
    }

    pub fn get_handlers(&mut self, command: &str) -> (CommandHandler, Vec<AfterHandler>) {
        let command_handler = if let Some(handler) = self.command_handlers.get_mut(command) {
            Rc::clone(handler)
        } else {
            panic!("No handlers for command: {command}");
        };
        let after_handlers = self.after_handlers.values().cloned().collect();

        (command_handler, after_handlers)
    }

    pub fn clear_all(&mut self) {
        self.command_handlers.clear();
        self.after_handlers.clear();
    }
}

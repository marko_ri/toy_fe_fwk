mod todo_create;
use todo_create::new_todo_create;
mod todo_list;
use todo_list::new_todo_list;
mod todo_item;
use todo_item::new_todo_item;

use crate::{Chandler, Component, Cstate, Payload, Vdom};
use std::{cell::RefCell, collections::HashMap, rc::Rc};

const ROOT: &str = "root";
const TODO_ITEM: &str = "todo_item";
const TODO_LIST: &str = "todo_list";
const TODO_CREATE: &str = "todo_create";

#[derive(Debug, Clone, PartialEq)]
struct Todo {
    pub id: usize,
    text: String,
}

#[derive(Debug, Clone, PartialEq)]
struct RootState {
    next_id: usize,
    todos: Vec<Todo>,
}

impl Cstate for RootState {
    fn as_any(&self) -> &dyn std::any::Any {
        self
    }

    fn clone(&self) -> Box<dyn Cstate> {
        Box::new(std::clone::Clone::clone(self))
    }

    fn equals(&self, other: &dyn Cstate) -> bool {
        other
            .as_any()
            .downcast_ref::<Self>()
            .map(|o| o == self)
            .unwrap_or(false)
    }
}

impl Default for RootState {
    fn default() -> Self {
        Self {
            next_id: 3,
            todos: vec![
                Todo {
                    id: 0,
                    text: "Walk the dog".to_string(),
                },
                Todo {
                    id: 1,
                    text: "Water the plants".to_string(),
                },
                Todo {
                    id: 2,
                    text: "Sand the chairs".to_string(),
                },
            ],
        }
    }
}

pub fn root_component() -> Rc<RefCell<Component>> {
    let mut event_handlers: HashMap<String, Chandler> = HashMap::new();
    event_handlers.insert(format!("{ROOT}_add"), Rc::new(event_add_todo));
    event_handlers.insert(format!("{ROOT}_edit"), Rc::new(event_edit_todo));
    event_handlers.insert(format!("{ROOT}_remove"), Rc::new(event_remove_todo));

    let view_fn = Box::new(move |state: &dyn Cstate| -> Vdom {
        let state = state.as_any().downcast_ref::<RootState>().unwrap();
        if state.todos.is_empty() {
            Vdom::fragment(vec![
                Vdom::element("h1", vec![], vec![Vdom::text("My TODOs")]),
                Vdom::component(new_todo_create(ROOT)),
            ])
        } else {
            Vdom::fragment(vec![
                Vdom::element("h1", vec![], vec![Vdom::text("My TODOs")]),
                Vdom::component(new_todo_create(ROOT)),
                Vdom::component(new_todo_list(ROOT, &state.todos)),
            ])
        }
    });

    Rc::new(RefCell::new(Component::new(
        ROOT,
        None,
        Box::<RootState>::default(),
        event_handlers,
        view_fn,
    )))
}

fn event_add_todo(component: Rc<RefCell<Component>>, payload: Payload) {
    let new_state = {
        let component_rc = component.borrow();
        let old = component_rc
            .state()
            .as_any()
            .downcast_ref::<RootState>()
            .unwrap();
        let todo = Todo {
            id: old.next_id,
            text: payload.string(),
        };
        let mut todos = old.todos.clone();
        todos.push(todo);
        Box::new(RootState {
            next_id: old.next_id + 1,
            todos,
        })
    };
    Component::set_state(&component, new_state);
}

fn event_edit_todo(component: Rc<RefCell<Component>>, payload: Payload) {
    let new_state = {
        let component_rc = component.borrow();
        let old = component_rc
            .state()
            .as_any()
            .downcast_ref::<RootState>()
            .unwrap();
        let mut pmap = payload.map();
        let todo_id = pmap.remove("i").unwrap().usize();
        let edited = pmap.remove("edited").unwrap().string();
        let mut new_todos = old.todos.clone();
        for todo in new_todos.iter_mut() {
            if todo.id == todo_id {
                todo.text = edited;
                break;
            }
        }
        Box::new(RootState {
            next_id: old.next_id,
            todos: new_todos,
        })
    };
    Component::set_state(&component, new_state);
}

fn event_remove_todo(component: Rc<RefCell<Component>>, payload: Payload) {
    let new_state = {
        let component_rc = component.borrow();
        let old = component_rc
            .state()
            .as_any()
            .downcast_ref::<RootState>()
            .unwrap();
        let mut pmap = payload.map();
        let todo_id = pmap.remove("i").unwrap().usize();
        let new_todos = old
            .todos
            .iter()
            .filter(|todo| todo.id != todo_id)
            .cloned()
            .collect();
        Box::new(RootState {
            next_id: old.next_id,
            todos: new_todos,
        })
    };
    Component::set_state(&component, new_state);
}

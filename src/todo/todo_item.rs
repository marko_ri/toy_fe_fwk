use super::{Todo, TODO_ITEM};
use crate::{bubbleup_event, Chandler, Component, Cstate, Payload, Vdom, Vproperty, HANDLERS};
use std::{cell::RefCell, collections::HashMap, rc::Rc};
use wasm_bindgen::JsCast;

#[derive(Default, Debug, Clone, PartialEq)]
struct TodoItemState {
    original: String,
    edited: String,
    is_editing: bool,
    i: usize,
}

impl TodoItemState {
    fn new(todo: &Todo) -> Self {
        Self {
            original: todo.text.to_string(),
            edited: todo.text.to_string(),
            is_editing: false,
            i: todo.id,
        }
    }
}

impl Cstate for TodoItemState {
    fn as_any(&self) -> &dyn std::any::Any {
        self
    }

    fn clone(&self) -> Box<dyn Cstate> {
        Box::new(std::clone::Clone::clone(self))
    }

    fn equals(&self, other: &dyn Cstate) -> bool {
        other
            .as_any()
            .downcast_ref::<Self>()
            .map(|o| o == self)
            .unwrap_or(false)
    }
}

pub fn new_todo_item(parent: &str, todo: &Todo) -> Rc<RefCell<Component>> {
    let id = format!("{}{}", TODO_ITEM, todo.id);
    let mut event_handlers: HashMap<String, Chandler> = HashMap::new();
    event_handlers.insert(format!("{id}_remove"), Rc::new(event_remove_todo));
    event_handlers.insert(format!("{id}_edit"), Rc::new(event_edit_todo));

    let view_fn = Box::new(move |state: &dyn Cstate| -> Vdom {
        let state = state.as_any().downcast_ref::<TodoItemState>().unwrap();
        if state.is_editing {
            Vdom::element(
                "li",
                vec![],
                vec![
                    Vdom::element(
                        "input",
                        vec![
                            Vproperty::Value(Some(state.edited.clone())),
                            Vproperty::Listener((
                                "input".to_string(),
                                Rc::new(listener_update_input),
                            )),
                        ],
                        vec![],
                    ),
                    Vdom::element(
                        "button",
                        vec![Vproperty::Listener((
                            "click".to_string(),
                            Rc::new(listener_save),
                        ))],
                        vec![Vdom::text("Save")],
                    ),
                    Vdom::element(
                        "button",
                        vec![Vproperty::Listener((
                            "click".to_string(),
                            Rc::new(listener_cancel),
                        ))],
                        vec![Vdom::text("Cancel")],
                    ),
                ],
            )
        } else {
            Vdom::element(
                "li",
                vec![],
                vec![
                    Vdom::element(
                        "span",
                        vec![Vproperty::Listener((
                            "dblclick".to_string(),
                            Rc::new(listener_update_state),
                        ))],
                        vec![Vdom::text(&state.original)],
                    ),
                    Vdom::element(
                        "button",
                        vec![Vproperty::Listener((
                            "click".to_string(),
                            Rc::new(listener_done),
                        ))],
                        vec![Vdom::text("Done")],
                    ),
                ],
            )
        }
    });

    Rc::new(RefCell::new(Component::new(
        &id,
        Some(parent.to_string()),
        Box::new(TodoItemState::new(todo)),
        event_handlers,
        view_fn,
    )))
}

fn listener_update_input(component: Rc<RefCell<Component>>, event: web_sys::Event) {
    let ev_target: web_sys::HtmlInputElement = event.target().unwrap().dyn_into().unwrap();
    let new_state = {
        let component_rc = component.borrow();
        let old_state = component_rc
            .state()
            .as_any()
            .downcast_ref::<TodoItemState>()
            .unwrap();
        Box::new(TodoItemState {
            edited: ev_target.value(),
            is_editing: old_state.is_editing,
            original: old_state.original.clone(),
            i: old_state.i,
        })
    };
    Component::set_state(&component, new_state);
}

fn listener_save(component: Rc<RefCell<Component>>, _event: web_sys::Event) {
    let (new_state, payload, id) = {
        let component_rc = component.borrow();
        let old_state = component_rc
            .state()
            .as_any()
            .downcast_ref::<TodoItemState>()
            .unwrap();
        let mut payload = HashMap::new();
        payload.insert(
            "edited".to_string(),
            Payload::String(old_state.edited.clone()),
        );
        payload.insert("i".to_string(), Payload::Usize(old_state.i));

        let new_state = Box::new(TodoItemState {
            edited: old_state.edited.clone(),
            is_editing: false,
            original: old_state.original.clone(),
            i: old_state.i,
        });
        let id = component_rc.id().to_string();

        (new_state, payload, id)
    };
    Component::set_state(&component, new_state);
    let (command_handler, _after_handlers) =
        HANDLERS.with_borrow_mut(|h| h.get_handlers(&format!("{id}_edit")));
    command_handler(Payload::Map(payload));
}

fn listener_cancel(component: Rc<RefCell<Component>>, _event: web_sys::Event) {
    let new_state = {
        let component_rc = component.borrow();
        let old_state = component_rc
            .state()
            .as_any()
            .downcast_ref::<TodoItemState>()
            .unwrap();
        Box::new(TodoItemState {
            edited: old_state.original.clone(),
            is_editing: false,
            original: old_state.original.clone(),
            i: old_state.i,
        })
    };
    Component::set_state(&component, new_state);
}

fn listener_update_state(component: Rc<RefCell<Component>>, _event: web_sys::Event) {
    let new_state = {
        let component_rc = component.borrow();
        let old_state = component_rc
            .state()
            .as_any()
            .downcast_ref::<TodoItemState>()
            .unwrap();
        Box::new(TodoItemState {
            edited: old_state.edited.clone(),
            is_editing: true,
            original: old_state.original.clone(),
            i: old_state.i,
        })
    };
    Component::set_state(&component, new_state);
}

fn listener_done(component: Rc<RefCell<Component>>, _event: web_sys::Event) {
    let (payload, id) = {
        let component_rc = component.borrow();
        let old_state = component_rc
            .state()
            .as_any()
            .downcast_ref::<TodoItemState>()
            .unwrap();
        let mut payload = HashMap::new();
        payload.insert("i".to_string(), Payload::Usize(old_state.i));
        let id = component_rc.id().to_string();

        (payload, id)
    };

    let (command_handler, _after_handlers) =
        HANDLERS.with_borrow_mut(|h| h.get_handlers(&format!("{id}_remove")));
    command_handler(Payload::Map(payload));
}

fn event_remove_todo(component: Rc<RefCell<Component>>, payload: Payload) {
    bubbleup_event("remove", component, payload);
}

fn event_edit_todo(component: Rc<RefCell<Component>>, payload: Payload) {
    bubbleup_event("edit", component, payload);
}

use super::TODO_CREATE;
use crate::{bubbleup_event, Chandler, Component, Cstate, Payload, Vdom, Vproperty, HANDLERS};
use std::{cell::RefCell, collections::HashMap, rc::Rc};
use wasm_bindgen::JsCast;

#[derive(Default, Debug, Clone, PartialEq)]
struct TodoCreateState {
    text: String,
}

impl Cstate for TodoCreateState {
    fn as_any(&self) -> &dyn std::any::Any {
        self
    }

    fn clone(&self) -> Box<dyn Cstate> {
        Box::new(std::clone::Clone::clone(self))
    }

    fn equals(&self, other: &dyn Cstate) -> bool {
        other
            .as_any()
            .downcast_ref::<Self>()
            .map(|o| o == self)
            .unwrap_or(false)
    }
}

pub fn new_todo_create(parent: &str) -> Rc<RefCell<Component>> {
    let mut event_handlers: HashMap<String, Chandler> = HashMap::new();
    event_handlers.insert(format!("{TODO_CREATE}_add"), Rc::new(event_add_todo));

    let view_fn = Box::new(|state: &dyn Cstate| -> Vdom {
        let state = state.as_any().downcast_ref::<TodoCreateState>().unwrap();
        Vdom::element(
            "div",
            vec![],
            vec![
                Vdom::element(
                    "label",
                    vec![Vproperty::Data((
                        "for".to_string(),
                        Some("todo-input".to_string()),
                    ))],
                    vec![Vdom::text("New TODO")],
                ),
                Vdom::element(
                    "input",
                    vec![
                        Vproperty::Type(Some("text".to_string())),
                        Vproperty::Id(Some("todo-input".to_string())),
                        Vproperty::Value(Some(state.text.clone())),
                        Vproperty::Listener(("input".to_string(), Rc::new(listener_on_input))),
                    ],
                    vec![],
                ),
                Vdom::element(
                    "button",
                    vec![
                        Vproperty::Disabled(Some(state.text.len() < 3)),
                        Vproperty::Listener(("click".to_string(), Rc::new(listener_add))),
                    ],
                    vec![Vdom::text("Add")],
                ),
            ],
        )
    });

    Rc::new(RefCell::new(Component::new(
        TODO_CREATE,
        Some(parent.to_string()),
        Box::<TodoCreateState>::default(),
        event_handlers,
        view_fn,
    )))
}

fn listener_on_input(component: Rc<RefCell<Component>>, event: web_sys::Event) {
    let ev_target: web_sys::HtmlInputElement = event.target().unwrap().dyn_into().unwrap();
    let new_state = Box::new(TodoCreateState {
        text: ev_target.value(),
    });

    Component::set_state(&component, new_state);
}

fn listener_add(component: Rc<RefCell<Component>>, _event: web_sys::Event) {
    let (new_state, old_text, id) = {
        let component_rc = component.borrow();
        let old = component_rc
            .state()
            .as_any()
            .downcast_ref::<TodoCreateState>()
            .unwrap();
        let old_text = old.text.clone();
        let new_state = Box::new(TodoCreateState {
            text: "".to_string(),
        });
        let id = component_rc.id().to_string();

        (new_state, old_text, id)
    };

    let (command_handler, _after_handlers) =
        HANDLERS.with_borrow_mut(|h| h.get_handlers(&format!("{id}_add")));
    command_handler(Payload::String(old_text));

    Component::set_state(&component, new_state);
}

fn event_add_todo(component: Rc<RefCell<Component>>, payload: Payload) {
    bubbleup_event("add", component, payload);
}

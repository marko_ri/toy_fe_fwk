use super::{new_todo_item, Todo, TODO_LIST};
use crate::{bubbleup_event, Chandler, Component, Cstate, Payload, Vdom};
use std::{cell::RefCell, collections::HashMap, rc::Rc};

#[derive(Default, Debug, Clone, PartialEq)]
pub struct TodoListState {
    pub todos: Vec<Todo>,
}

impl TodoListState {
    fn new(todos: &[Todo]) -> Self {
        Self {
            todos: todos.to_vec(),
        }
    }
}

impl Cstate for TodoListState {
    fn as_any(&self) -> &dyn std::any::Any {
        self
    }

    fn clone(&self) -> Box<dyn Cstate> {
        Box::new(std::clone::Clone::clone(self))
    }

    fn equals(&self, other: &dyn Cstate) -> bool {
        other
            .as_any()
            .downcast_ref::<Self>()
            .map(|o| o == self)
            .unwrap_or(false)
    }
}

pub fn new_todo_list(parent: &str, todos: &[Todo]) -> Rc<RefCell<Component>> {
    let mut event_handlers: HashMap<String, Chandler> = HashMap::new();
    event_handlers.insert(format!("{TODO_LIST}_remove"), Rc::new(event_remove_todo));
    event_handlers.insert(format!("{TODO_LIST}_edit"), Rc::new(event_edit_todo));

    let view_fn = Box::new(move |state: &dyn Cstate| -> Vdom {
        let state = state.as_any().downcast_ref::<TodoListState>().unwrap();
        Vdom::element(
            "ul",
            vec![],
            state
                .todos
                .iter()
                .map(|todo| Vdom::component(new_todo_item(TODO_LIST, todo)))
                .collect(),
        )
    });

    Rc::new(RefCell::new(Component::new(
        TODO_LIST,
        Some(parent.to_string()),
        Box::new(TodoListState::new(todos)),
        event_handlers,
        view_fn,
    )))
}

fn event_remove_todo(component: Rc<RefCell<Component>>, payload: Payload) {
    bubbleup_event("remove", component, payload);
}

fn event_edit_todo(component: Rc<RefCell<Component>>, payload: Payload) {
    bubbleup_event("edit", component, payload)
}

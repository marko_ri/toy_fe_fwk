// TODO: code should be tranformed to be more "rusty" like
// At the moment it is more "javascripty" like (and probably leaking mem)
// debugging is painful

mod utils;
pub use utils::{
    add_event_listeners, create_element, diff_props, diff_vdoms, find_index_in_parent,
    get_element_by_id, set_attribute, set_attributes, DiffProps, Operation, OperationKind,
};
mod vdom;
pub use vdom::{Listener, Vdom, VdomKind, Velement, Vproperty};
mod component;
pub use component::{Component, Cstate};
mod handler_registry;
pub use handler_registry::{HandlerRegistry, Payload};
mod app;
pub use app::{mount_app, unmount_app};
mod destroy_dom;
pub use destroy_dom::destroy_dom;
mod mount_dom;
pub use mount_dom::mount_dom;
mod patch_dom;
pub use patch_dom::patch_dom;
// app specific
mod todo;
use todo::root_component;

use std::{cell::RefCell, rc::Rc};
use wasm_bindgen::prelude::*;

// notify renderer about state changes
pub type CommandHandler = Rc<dyn Fn(Payload)>;
// called after every command is handled
pub type AfterHandler = Rc<dyn Fn()>;
// component EventHandler
pub type Chandler = Rc<dyn Fn(Rc<RefCell<Component>>, Payload)>;
// Vproperty::Listener handler
pub type Vhandler = Rc<dyn Fn(Rc<RefCell<Component>>, web_sys::Event)>;
// syntactic sugar
pub type VdomRc = Rc<RefCell<Vdom>>;
// ViewFn is called from component.render
pub type ViewFn = Box<dyn Fn(&dyn Cstate) -> Vdom>;

thread_local! {
    pub static HANDLERS: RefCell<HandlerRegistry> = RefCell::new(HandlerRegistry::default());
}

// TODO: error handling
#[wasm_bindgen(start)]
fn main_js() {
    console_error_panic_hook::set_once();

    let root = root_component();
    mount_app(root);
}

pub fn bubbleup_event(name: &str, component: Rc<RefCell<Component>>, payload: Payload) {
    let parent_id = component.borrow().parent_id().to_string();
    let event_name = format!("{parent_id}_{name}");
    let (command_handler, _after_handlers) =
        HANDLERS.with_borrow_mut(|h| h.get_handlers(&event_name));
    command_handler(payload);
}

use crate::{create_element, Component};
use std::{cell::RefCell, rc::Rc};

pub fn mount_app(root: Rc<RefCell<Component>>) {
    let body = web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .body()
        .unwrap();
    let parent = create_element("div").unwrap();
    body.append_child(&parent).unwrap();
    Component::mount(&root, &parent, None);
}

pub fn unmount_app(root: Rc<RefCell<Component>>) {
    root.borrow_mut().unmount();
}

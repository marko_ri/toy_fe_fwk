use crate::{
    add_event_listeners, create_element, set_attributes, Component, Listener, Vdom, VdomKind,
    Vproperty,
};
use std::{cell::RefCell, collections::HashMap, rc::Rc};
use wasm_bindgen::JsCast;

pub fn mount_dom(
    vdom: &mut Vdom,
    parent: &web_sys::Node,
    mut idx: Option<u32>,
    host_component: &Rc<RefCell<Component>>,
) {
    //web_sys::console::log_1(&format!("MOUNT: {vdom:?}, on: {}", parent.node_name()).into());
    match &vdom.kind {
        VdomKind::Text(val) => {
            let dom: web_sys::Node = web_sys::Text::new_with_data(val)
                .unwrap()
                .dyn_into()
                .unwrap();
            insert(&dom, parent, idx);
            vdom.node = Some(dom);
        }
        VdomKind::Element(val) => {
            let dom = create_element(val.tag()).unwrap();
            let listeners = add_props(&dom, val.props(), host_component);
            for child in val.children().iter() {
                mount_dom(&mut child.borrow_mut(), &dom, idx, host_component);
            }
            insert(&dom, parent, idx);
            vdom.node = Some(dom.dyn_into().unwrap());
            vdom.set_listeners(listeners);
        }
        VdomKind::Fragment(children) => {
            for child in children.iter() {
                mount_dom(&mut child.borrow_mut(), parent, idx, host_component);

                if idx.is_none() {
                    continue;
                }

                match &child.borrow().kind {
                    VdomKind::Fragment(children) => {
                        *idx.as_mut().unwrap() += children.len() as u32;
                    }
                    _ => {
                        *idx.as_mut().unwrap() += 1;
                    }
                }
            }
            vdom.node = Some(parent.clone());
        }
        VdomKind::Component(val) => {
            let val = Rc::clone(val);
            Component::mount(&val, parent, idx);
            let component = val.borrow();
            let node: Option<web_sys::Node> =
                component.first_element().map(|e| e.dyn_into().unwrap());
            vdom.node = node;
        }
    }
}

fn add_props(
    el: &web_sys::Element,
    props: &[Vproperty],
    host_component: &Rc<RefCell<Component>>,
) -> Vec<Listener> {
    let mut listeners = HashMap::new();
    let mut attributes = Vec::new();
    for prop in props.iter().cloned() {
        match prop {
            Vproperty::Listener((event_name, handler)) => {
                listeners.insert(event_name, handler);
            }
            _ => {
                attributes.push(prop);
            }
        }
    }
    set_attributes(el, attributes);
    add_event_listeners(el, listeners, host_component)
}

fn insert(el: &web_sys::Node, parent_el: &web_sys::Node, idx: Option<u32>) {
    if let Some(idx) = idx {
        let children = parent_el.child_nodes();
        if idx >= children.length() {
            parent_el.append_child(el).unwrap();
        } else {
            parent_el
                .insert_before(el, children.get(idx).as_ref())
                .unwrap();
        }
    } else {
        parent_el.append_child(el).unwrap();
    }
}

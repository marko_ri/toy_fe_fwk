use crate::{VdomRc, Vproperty};
use std::rc::Rc;

#[derive(Debug)]
pub struct DiffProps {
    pub added: Vec<Vproperty>,
    pub removed: Vec<Vproperty>,
    pub updated: Vec<Vproperty>,
}

pub fn diff_props(old_props: &[&Vproperty], new_props: &[&Vproperty]) -> DiffProps {
    let updated: Vec<Vproperty> = new_props
        .iter()
        .filter(|new| old_props.iter().any(|old| new.is_updated(old)))
        .map(|&new| new.clone())
        .collect();

    let (added, removed) = if updated.is_empty() {
        let added: Vec<Vproperty> = new_props
            .iter()
            .filter(|new| !old_props.iter().any(|old| &old == new))
            .map(|&new| new.clone())
            .collect();
        let removed: Vec<Vproperty> = old_props
            .iter()
            .filter(|old| !new_props.iter().any(|new| &new == old))
            .map(|&old| old.clone())
            .collect();

        (added, removed)
    } else {
        (vec![], vec![])
    };

    DiffProps {
        added,
        removed,
        updated,
    }
}

#[derive(Debug)]
pub enum OperationKind {
    Add,
    Remove,
    Move,
    Noop,
}

#[derive(Debug)]
pub struct Operation {
    pub kind: OperationKind,
    // in the move and noop operations, the originalIndex indicates the index
    // where the item started.
    pub original_index: Option<i32>,
    // in the move operation, we also keep track of the from property,
    // which is the index from which the item was moved
    pub from: Option<usize>,
    // index where the item ends up, except in the case of a remove operation,
    // where it’s the index from which the item was removed
    pub idx: usize,
    pub item: VdomRc,
}

#[derive(Debug)]
struct ArrayWithOriginalIndices {
    array: Vec<VdomRc>,
    original_indices: Vec<i32>,
}

impl ArrayWithOriginalIndices {
    fn new(array: &[VdomRc]) -> Self {
        Self {
            array: array.to_vec(),
            original_indices: (0..array.len() as i32).collect(),
        }
    }

    fn is_removal(&self, idx: usize, new_arr: &[VdomRc]) -> bool {
        if idx >= self.array.len() {
            return false;
        }

        let item = self.array[idx].borrow();
        !new_arr.iter().map(|v| v.borrow()).any(|v| *v == *item)
    }

    fn remove_item(&mut self, idx: usize) -> Operation {
        let result = Operation {
            kind: OperationKind::Remove,
            original_index: None,
            from: None,
            idx,
            item: self.array[idx].clone(),
        };
        self.array.remove(idx);
        self.original_indices.remove(idx);

        result
    }

    fn is_noop(&self, idx: usize, new_arr: &[VdomRc]) -> bool {
        if idx >= self.array.len() {
            return false;
        }

        let item = self.array[idx].borrow();
        let new_item = new_arr[idx].borrow();
        *item == *new_item
    }

    fn noop_item(&mut self, idx: usize) -> Operation {
        Operation {
            kind: OperationKind::Noop,
            original_index: Some(self.original_indices[idx]),
            from: None,
            idx,
            item: self.array[idx].clone(),
        }
    }

    fn is_addition(&self, item: &VdomRc, from_idx: usize) -> bool {
        self.find_index_from(item, from_idx).is_none()
    }

    fn find_index_from(&self, item: &VdomRc, from_idx: usize) -> Option<usize> {
        (from_idx..self.array.len()).find(|&i| *item.borrow() == *self.array[i].borrow())
    }

    fn add_item(&mut self, item: &VdomRc, idx: usize) -> Operation {
        let result = Operation {
            kind: OperationKind::Add,
            original_index: None,
            from: None,
            idx,
            item: Rc::clone(item),
        };
        self.array.insert(idx, Rc::clone(item));
        self.original_indices.insert(idx, -1);

        result
    }

    fn move_item(&mut self, item: &VdomRc, to_idx: usize) -> Operation {
        let from_idx = self.find_index_from(item, to_idx).unwrap();
        let result = Operation {
            kind: OperationKind::Move,
            original_index: Some(self.original_indices[from_idx]),
            from: Some(from_idx),
            idx: to_idx,
            item: self.array[from_idx].clone(),
        };

        let old_item = self.array.remove(from_idx);
        self.array.insert(to_idx, old_item);

        let orig_idx = self.original_indices.remove(from_idx);
        self.original_indices.insert(to_idx, orig_idx);

        result
    }

    fn remove_items_after(&mut self, idx: usize) -> Vec<Operation> {
        let mut result = Vec::new();
        while self.array.len() > idx {
            result.push(self.remove_item(idx));
        }
        result
    }
}

pub fn diff_vdoms(old_arr: &[VdomRc], new_arr: &[VdomRc]) -> Vec<Operation> {
    let mut result = Vec::new();
    let mut array = ArrayWithOriginalIndices::new(old_arr);
    let mut idx = 0;
    while idx < new_arr.len() {
        if array.is_removal(idx, new_arr) {
            let removed = array.remove_item(idx);
            // web_sys::console::log_1(&format!("REMOVAL: {removed:?}").into());
            result.push(removed);
            continue;
        }

        if array.is_noop(idx, new_arr) {
            let nooped = array.noop_item(idx);
            // web_sys::console::log_1(&format!("NOOP: {nooped:?}").into());
            result.push(nooped);
            idx += 1;
            continue;
        }

        let item = &new_arr[idx];

        if array.is_addition(item, idx) {
            let added = array.add_item(item, idx);
            // web_sys::console::log_1(&format!("ADDITION: {added:?}").into());
            result.push(added);
            idx += 1;
            continue;
        }

        let moved = array.move_item(item, idx);
        // web_sys::console::log_1(&format!("MOVE: {moved:?}").into());
        result.push(moved);
        idx += 1;
    }
    result.extend(array.remove_items_after(new_arr.len()));

    result
}

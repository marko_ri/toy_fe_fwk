mod attributes;
pub use attributes::*;
mod diff_props;
pub use diff_props::*;
use wasm_bindgen::JsCast;

pub fn get_element_by_id<T>(selector: &str) -> Option<T>
where
    T: JsCast,
{
    web_sys::window()?
        .document()?
        .get_element_by_id(selector)?
        .dyn_into::<T>()
        .ok()
}

pub fn create_element(tag: &str) -> Option<web_sys::Element> {
    web_sys::window()?.document()?.create_element(tag).ok()
}

pub fn find_index_in_parent(parent: &web_sys::Node, el: Option<&web_sys::Node>) -> Option<u32> {
    let children = parent.child_nodes();
    (0..children.length()).find(|&i| children.get(i).as_ref() == el)
}

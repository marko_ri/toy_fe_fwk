use crate::{Component, Listener, Vhandler, Vproperty};
use std::{cell::RefCell, collections::HashMap, rc::Rc};
use wasm_bindgen::JsCast;

pub fn set_attributes(el: &web_sys::Element, props: Vec<Vproperty>) {
    for prop in props.into_iter() {
        set_attribute(el, prop);
    }
}

pub fn set_attribute(el: &web_sys::Element, prop: Vproperty) {
    match prop {
        Vproperty::Id(val) => {
            let val = val.as_deref().unwrap_or_default();
            el.set_id(val);
        }
        Vproperty::Class(class) => {
            if let Some(class) = class {
                for class in class.split_whitespace() {
                    el.class_list().add_1(class).unwrap();
                }
            } else {
                el.class_list().set_value("");
            }
        }
        Vproperty::Style((prop, val)) => {
            let el: &web_sys::HtmlElement = el.dyn_ref().unwrap();
            if let Some(val) = val {
                el.style().set_property(&prop, &val).unwrap();
            } else {
                el.style().remove_property(&prop).unwrap();
            }
        }
        Vproperty::Data((name, val)) => {
            if let Some(val) = val {
                el.set_attribute(&name, &val).unwrap();
            } else {
                el.remove_attribute(&name).unwrap();
            }
        }
        Vproperty::Value(val) => {
            let val = val.as_deref().unwrap_or_default();
            let el: &web_sys::HtmlInputElement = el.dyn_ref().unwrap();
            el.set_value(val);
        }
        Vproperty::ValueNum(val) => {
            let val = val.unwrap_or_default();
            let el: &web_sys::HtmlInputElement = el.dyn_ref().unwrap();
            el.set_value_as_number(val);
        }
        Vproperty::Type(val) => {
            let val = val.as_deref().unwrap_or("text");
            let el: &web_sys::HtmlInputElement = el.dyn_ref().unwrap();
            el.set_type(val);
        }
        Vproperty::Size(val) => {
            let val = val.unwrap_or(20);
            let el: &web_sys::HtmlInputElement = el.dyn_ref().unwrap();
            el.set_size(val);
        }
        Vproperty::Name(val) => {
            let val = val.as_deref().unwrap_or_default();
            let el: &web_sys::HtmlInputElement = el.dyn_ref().unwrap();
            el.set_name(val);
        }
        Vproperty::Checked(val) => {
            let val = val.unwrap_or_default();
            let el: &web_sys::HtmlInputElement = el.dyn_ref().unwrap();
            el.set_checked(val);
        }
        Vproperty::Disabled(val) => {
            let val = val.unwrap_or_default();
            if el.has_type::<web_sys::HtmlButtonElement>() {
                let el: &web_sys::HtmlButtonElement = el.dyn_ref().unwrap();
                el.set_disabled(val);
            } else {
                let el: &web_sys::HtmlInputElement = el.dyn_ref().unwrap();
                el.set_disabled(val);
            }
        }
        Vproperty::Selected(val) => {
            let val = val.unwrap_or_default();
            let el: &web_sys::HtmlSelectElement = el.dyn_ref().unwrap();
            el.set_selected_index(val);
        }
        Vproperty::Listener(_) => unreachable!(),
    }
}

pub fn add_event_listeners(
    element: &web_sys::EventTarget,
    listeners: HashMap<String, Vhandler>,
    host_component: &Rc<RefCell<Component>>,
) -> Vec<Listener> {
    listeners
        .into_iter()
        .map(|(event_name, handler)| Listener::new(element, event_name, handler, host_component))
        .collect()
}

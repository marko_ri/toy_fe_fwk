use crate::{Component, VdomRc, Vhandler};
use std::{cell::RefCell, collections::HashSet, rc::Rc};
use wasm_bindgen::{closure::Closure, JsCast};

#[derive(Default, Clone)]
pub struct Vdom {
    pub kind: VdomKind,
    pub node: Option<web_sys::Node>,
    listeners: Vec<Listener>,
}

impl std::cmp::PartialEq for Vdom {
    fn eq(&self, other: &Self) -> bool {
        match (&self.kind, &other.kind) {
            (VdomKind::Text(_), VdomKind::Text(_)) => true,
            (VdomKind::Fragment(_), VdomKind::Fragment(_)) => true,
            (VdomKind::Element(e1), VdomKind::Element(e2)) => e1.tag() == e2.tag(),
            (VdomKind::Component(c1), VdomKind::Component(c2)) => *c1.borrow() == *c2.borrow(),
            _ => false,
        }
    }
}

impl std::fmt::Debug for Vdom {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Vdom")
            .field("kind", &self.kind)
            .field("node", &self.node.as_ref().map(|n| n.node_name()))
            .field(
                "listeners",
                &self.listeners.iter().map(|l| l.name()).collect::<Vec<_>>(),
            )
            .finish()
    }
}

impl Vdom {
    fn new(kind: VdomKind) -> Self {
        Self {
            kind,
            node: None,
            listeners: vec![],
        }
    }

    pub fn text(value: &str) -> Self {
        Self::new(VdomKind::text(value))
    }

    pub fn fragment(vnodes: Vec<Vdom>) -> Self {
        Self::new(VdomKind::fragment(vnodes))
    }

    pub fn element(tag: &str, props: Vec<Vproperty>, children: Vec<Vdom>) -> Self {
        Self::new(VdomKind::element(tag, props, children))
    }

    pub fn component(component: Rc<RefCell<Component>>) -> Self {
        Self::new(VdomKind::component(component))
    }

    pub fn set_listeners(&mut self, listeners: Vec<Listener>) {
        self.listeners = listeners;
    }

    pub fn clear_listeners(&mut self) {
        self.listeners.clear();
    }

    pub fn remove_listeners(&mut self, to_remove: HashSet<String>) {
        self.listeners.retain(|l| !to_remove.contains(l.name()));
    }

    pub fn remove_node_from_dom(&self) {
        let node = self.node.as_ref().unwrap();
        if node.has_type::<web_sys::Element>() {
            node.dyn_ref::<web_sys::Element>().unwrap().remove();
        } else {
            let parent = node.parent_element().unwrap();
            parent.remove_child(node).unwrap();
        }
    }

    pub fn drop_node(&mut self) {
        self.node = None;
    }

    pub fn extract_children(&self) -> Vec<VdomRc> {
        match &self.kind {
            VdomKind::Text(_) => vec![],
            VdomKind::Element(el) => extract_children_helper(el.children()),
            VdomKind::Fragment(children) => extract_children_helper(children),
            VdomKind::Component(_) => vec![],
        }
    }

    pub fn elements(&self) -> Vec<web_sys::Node> {
        let Some(node) = self.node.as_ref() else {
            return vec![];
        };
        if let VdomKind::Fragment(_) = &self.kind {
            let mut result = Vec::new();
            for child_rc in self.extract_children().iter() {
                let child = child_rc.borrow();
                if let VdomKind::Component(_) = &child.kind {
                    result.extend(child.elements());
                } else if let Some(node) = &child.node {
                    result.push(node.clone());
                }
            }
            result
        } else {
            vec![node.clone()]
        }
    }
}

fn extract_children_helper(children: &[VdomRc]) -> Vec<VdomRc> {
    let mut result = vec![];
    for child in children.iter() {
        match &child.borrow().kind {
            VdomKind::Text(_) => result.push(Rc::clone(child)),
            VdomKind::Element(_) => result.push(Rc::clone(child)),
            VdomKind::Fragment(_) => result.extend(child.borrow().extract_children()),
            VdomKind::Component(_) => result.push(Rc::clone(child)),
        }
    }
    result
}

#[derive(PartialEq, Debug, Clone)]
pub enum VdomKind {
    Text(String),
    Element(Velement),
    Fragment(Vec<VdomRc>),
    Component(Rc<RefCell<Component>>),
}

impl Default for VdomKind {
    fn default() -> Self {
        VdomKind::Text("".to_string())
    }
}

impl VdomKind {
    pub fn text(value: &str) -> Self {
        Self::Text(value.to_string())
    }

    pub fn fragment(vnodes: Vec<Vdom>) -> Self {
        let vnodes = vnodes
            .into_iter()
            .map(|v| Rc::new(RefCell::new(v)))
            .collect();
        Self::Fragment(vnodes)
    }

    pub fn element(tag: &str, props: Vec<Vproperty>, children: Vec<Vdom>) -> Self {
        let children = children
            .into_iter()
            .map(|v| Rc::new(RefCell::new(v)))
            .collect();
        Self::Element(Velement {
            tag: tag.to_string(),
            props,
            children,
        })
    }

    pub fn component(component: Rc<RefCell<Component>>) -> Self {
        Self::Component(component)
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Velement {
    tag: String,
    props: Vec<Vproperty>,
    children: Vec<VdomRc>,
}

impl Velement {
    pub fn tag(&self) -> &str {
        &self.tag
    }

    pub fn props(&self) -> &[Vproperty] {
        &self.props
    }

    pub fn children(&self) -> &[VdomRc] {
        &self.children
    }
}

#[derive(Clone)]
pub enum Vproperty {
    Listener((String, Vhandler)),
    Id(Option<String>),
    Class(Option<String>),
    Style((String, Option<String>)),
    Data((String, Option<String>)),
    Value(Option<String>),
    ValueNum(Option<f64>),
    Type(Option<String>),
    Size(Option<u32>),
    Name(Option<String>),
    Checked(Option<bool>),
    Disabled(Option<bool>),
    Selected(Option<i32>),
}

impl std::fmt::Debug for Vproperty {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use Vproperty::*;
        match self {
            Id(x) => write!(f, "Id({x:?})"),
            Class(x) => write!(f, "Class({x:?})"),
            Style(x) => write!(f, "Style({x:?})"),
            Data(x) => write!(f, "Data({x:?})"),
            Value(x) => write!(f, "Value({x:?})"),
            ValueNum(x) => write!(f, "ValueNum({x:?})"),
            Type(x) => write!(f, "Type({x:?})"),
            Size(x) => write!(f, "Size({x:?})"),
            Name(x) => write!(f, "Name({x:?})"),
            Checked(x) => write!(f, "Checked({x:?})"),
            Disabled(x) => write!(f, "Disabled({x:?})"),
            Selected(x) => write!(f, "Selected({x:?})"),
            Listener((x, _)) => write!(f, "Listener({x:?})"),
        }
    }
}

impl Vproperty {
    pub fn clear_value(&self) -> Self {
        use Vproperty::*;
        match self {
            Id(_) => Id(None),
            Class(_) => Class(None),
            Style((name, _val)) => Style((name.to_string(), None)),
            Data((name, _val)) => Data((name.to_string(), None)),
            Value(_) => Value(None),
            ValueNum(_) => ValueNum(None),
            Type(_) => Type(None),
            Size(_) => Size(None),
            Name(_) => Name(None),
            Checked(_) => Checked(None),
            Disabled(_) => Disabled(None),
            Selected(_) => Selected(None),
            Listener(_) => unreachable!(),
        }
    }

    pub fn is_updated(&self, other: &Self) -> bool {
        use Vproperty::*;
        match (self, other) {
            (Id(a), Id(b)) => a != b,
            (Class(a), Class(b)) => a != b,
            (Style(a), Style(b)) => a != b,
            (Data(a), Data(b)) => a != b,
            (Value(a), Value(b)) => a != b,
            (ValueNum(a), ValueNum(b)) => a != b,
            (Type(a), Type(b)) => a != b,
            (Size(a), Size(b)) => a != b,
            (Name(a), Name(b)) => a != b,
            (Checked(a), Checked(b)) => a != b,
            (Disabled(a), Disabled(b)) => a != b,
            (Selected(a), Selected(b)) => a != b,
            // (Listener((a, _)), Listener((b, _)))
            _ => false,
        }
    }
}

impl std::cmp::PartialEq for Vproperty {
    fn eq(&self, other: &Self) -> bool {
        use Vproperty::*;
        match (self, other) {
            (Id(a), Id(b)) => a == b,
            (Class(a), Class(b)) => a == b,
            (Style(a), Style(b)) => a == b,
            (Data(a), Data(b)) => a == b,
            (Value(a), Value(b)) => a == b,
            (ValueNum(a), ValueNum(b)) => a == b,
            (Type(a), Type(b)) => a == b,
            (Size(a), Size(b)) => a == b,
            (Name(a), Name(b)) => a == b,
            (Checked(a), Checked(b)) => a == b,
            (Disabled(a), Disabled(b)) => a == b,
            (Selected(a), Selected(b)) => a == b,
            // (Listener((a, _)), Listener((b, _)))
            _ => false,
        }
    }
}

#[derive(Clone)]
pub struct Listener {
    element: web_sys::EventTarget,
    name: String,
    cb: Rc<Closure<dyn FnMut(web_sys::Event)>>,
}

impl Listener {
    pub fn new(
        element: &web_sys::EventTarget,
        name: String,
        handler: Vhandler,
        host_component: &Rc<RefCell<Component>>,
    ) -> Self {
        // web_sys::console::log_1(&format!("ADD_LISTENER: {name:?}").into());
        let host_component = Rc::clone(host_component);
        let cb = Closure::new(move |event: web_sys::Event| {
            handler(Rc::clone(&host_component), event);
        });
        element
            .add_event_listener_with_callback(&name, cb.as_ref().unchecked_ref())
            .unwrap();

        Self {
            element: element.clone(),
            name,
            cb: Rc::new(cb),
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }
}

impl Drop for Listener {
    fn drop(&mut self) {
        // web_sys::console::log_1(&format!("DROP_LISTENER: {:?}", self.name).into());
        self.element
            .remove_event_listener_with_callback(
                &self.name,
                self.cb.as_ref().as_ref().unchecked_ref(),
            )
            .unwrap();
    }
}

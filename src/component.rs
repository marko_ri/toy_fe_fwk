use crate::{destroy_dom, mount_dom, patch_dom, Chandler, Vdom, VdomKind, ViewFn, HANDLERS};
use std::{cell::RefCell, collections::HashMap, rc::Rc};
use wasm_bindgen::JsCast;

pub trait Cstate: std::fmt::Debug {
    fn as_any(&self) -> &dyn std::any::Any;
    fn clone(&self) -> Box<dyn Cstate>;
    fn equals(&self, other: &dyn Cstate) -> bool;
}

pub struct Component {
    id: String,
    parent_id: Option<String>,
    // TODO: parent component sets the state directly on child component
    // (i.e not through the props field)
    state: Box<dyn Cstate>,
    view_fn: ViewFn,
    pub vdom: Option<Vdom>,
    pub host_el: Option<web_sys::Node>,
    pub is_mounted: bool,
    // child component communicates with its parent via events
    event_handlers: HashMap<String, Chandler>,
}

impl std::cmp::PartialEq for Component {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl std::fmt::Debug for Component {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Component")
            .field("id", &self.id)
            .field("parent_id", &self.parent_id)
            .field("state", &self.state)
            .field("vdom", &self.vdom)
            .field("host_el", &self.host_el)
            .field("is_mounted", &self.is_mounted)
            .finish()
    }
}

impl Component {
    pub fn new(
        id: &str,
        parent_id: Option<String>,
        state: Box<dyn Cstate>,
        event_handlers: HashMap<String, Chandler>,
        view_fn: ViewFn,
    ) -> Self {
        Self {
            id: id.to_string(),
            parent_id,
            state,
            view_fn,
            vdom: None,
            host_el: None,
            is_mounted: false,
            event_handlers,
        }
    }

    pub fn id(&self) -> &str {
        &self.id
    }

    pub fn parent_id(&self) -> &str {
        self.parent_id.as_ref().unwrap()
    }

    pub fn set_state(component: &Rc<RefCell<Self>>, state: Box<dyn Cstate>) {
        {
            let mut component_rc = component.borrow_mut();
            if component_rc.state.equals(&*state) {
                return;
            }
            component_rc.state = state;
        }
        Self::patch(component);
    }

    pub fn state(&self) -> &dyn Cstate {
        &*self.state
    }

    pub fn first_element(&self) -> Option<web_sys::Element> {
        self.vdom
            .as_ref()
            .unwrap()
            .elements()
            .into_iter()
            .next()
            .map(|r| r.dyn_into().unwrap())
    }

    pub fn offset(&self) -> u32 {
        if self.parent_id.is_none() {
            return 0;
        }
        if let VdomKind::Fragment(_) = &self.vdom.as_ref().unwrap().kind {
            let first_element = self.first_element();
            let host_el: &web_sys::Element = self.host_el.as_ref().unwrap().dyn_ref().unwrap();
            for i in 0..host_el.children().length() {
                if host_el.children().item(i) == first_element {
                    return i;
                }
            }
        }
        0
    }

    pub fn render(&self) -> Vdom {
        (self.view_fn)(&*self.state)
    }

    pub fn mount(component: &Rc<RefCell<Self>>, host_el: &web_sys::Node, index: Option<u32>) {
        let mut vdom = {
            let component = component.borrow();
            if component.is_mounted {
                panic!("Component is already mounted");
            }
            component.render()
        };
        // passes "this" to the mount_dom function
        // another option is Rc::new_cyclic
        mount_dom(&mut vdom, host_el, index, component);
        Self::wire_event_handlers(component);
        let mut component = component.borrow_mut();
        component.vdom = Some(vdom);
        component.host_el = Some(host_el.clone());
        component.is_mounted = true;
    }

    pub fn unmount(&mut self) {
        destroy_dom(self.vdom.as_mut().unwrap());
        self.host_el = None;
        self.is_mounted = false;
        self.vdom = None;
        self.clear_event_handlers();
    }

    fn clear_event_handlers(&mut self) {
        HANDLERS.with_borrow_mut(|h| {
            for event_name in self.event_handlers.keys() {
                h.unsubscribe(event_name);
            }
        });
        self.event_handlers.clear();
    }

    fn wire_event_handlers(component: &Rc<RefCell<Self>>) {
        let event_handlers = component.borrow().event_handlers.clone();
        for (event_name, handler) in event_handlers.iter() {
            let component_rc = Rc::clone(component);
            let handler = Rc::clone(handler);
            let callback = Rc::new(move |payload| {
                // hehe, easier if we do not count rc.clones :)
                handler(Rc::clone(&component_rc), payload);
            });
            HANDLERS.with_borrow_mut(|h| h.subscribe(event_name, callback));
        }
    }

    pub fn patch(component: &Rc<RefCell<Self>>) {
        let (mut new_vdom, mut old_vdom, parent_node) = {
            let component = component.borrow();
            if !component.is_mounted {
                panic!("Component is not mounted");
            }
            let new_vdom = component.render();
            let parent_node = component.host_el.as_ref().unwrap().clone();
            let old_vdom = component.vdom.as_ref().unwrap().clone();

            (new_vdom, old_vdom, parent_node)
        };

        // passes "this" to the patch_dom function
        // another option is Rc::new_cyclic
        patch_dom(&mut old_vdom, &mut new_vdom, &parent_node, component);
        component.borrow_mut().vdom = Some(new_vdom);
    }
}

impl Drop for Component {
    fn drop(&mut self) {
        // web_sys::console::log_1(&format!("DROP_COMPONENT: {:?}", self.id).into());
    }
}

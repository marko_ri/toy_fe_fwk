use crate::{Vdom, VdomKind};

pub fn destroy_dom(vdom: &mut Vdom) {
    // web_sys::console::log_1(&format!("DESTROY: {vdom:?}").into());
    if vdom.node.is_none() {
        panic!("Can only destroy DOM nodes that have been mounted");
    }

    match &vdom.kind {
        VdomKind::Text(_) => {
            vdom.remove_node_from_dom();
        }
        VdomKind::Element(vel) => {
            for child in vel.children().iter() {
                destroy_dom(&mut child.borrow_mut());
            }
            vdom.clear_listeners();
            vdom.remove_node_from_dom();
        }
        VdomKind::Fragment(children) => {
            for child in children.iter() {
                destroy_dom(&mut child.borrow_mut());
            }
        }
        VdomKind::Component(vc) => {
            vc.borrow_mut().unmount();
        }
    }

    vdom.drop_node();
}

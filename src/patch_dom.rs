use crate::{
    add_event_listeners, destroy_dom, diff_props, diff_vdoms, find_index_in_parent, mount_dom,
    set_attribute, Component, Listener, OperationKind, Vdom, VdomKind, Vproperty,
};
use std::{
    cell::RefCell,
    collections::{HashMap, HashSet},
    rc::Rc,
};
use wasm_bindgen::JsCast;

pub fn patch_dom(
    old_vdom: &mut Vdom,
    new_vdom: &mut Vdom,
    parent: &web_sys::Node,
    host_component: &Rc<RefCell<Component>>,
) {
    // web_sys::console::log_1(&format!("PATCH_DOM: old: {old_vdom:?}, new: {new_vdom:?}").into());
    if old_vdom != new_vdom {
        let idx = find_index_in_parent(parent, old_vdom.node.as_ref());
        destroy_dom(old_vdom);
        mount_dom(new_vdom, parent, idx, host_component);
        return;
    }

    new_vdom.node.clone_from(&old_vdom.node);

    match (&old_vdom.kind, &new_vdom.kind) {
        (VdomKind::Text(old_text), VdomKind::Text(new_text)) => {
            let old_text = old_text.clone();
            let new_text = new_text.clone();
            if old_text != new_text {
                let el = new_vdom.node.as_mut().unwrap();
                el.set_node_value(Some(&new_text));
            }
        }
        (VdomKind::Element(old_el), VdomKind::Element(new_el)) => {
            let (new_listeners, remove_listener_names) = {
                let mut old_classes: Vec<&Vproperty> = Vec::new();
                let mut old_styles: Vec<&Vproperty> = Vec::new();
                let mut old_events: Vec<&Vproperty> = Vec::new();
                let mut old_attrs: Vec<&Vproperty> = Vec::new();
                for prop in old_el.props().iter() {
                    match prop {
                        Vproperty::Class(_) => old_classes.push(prop),
                        Vproperty::Style(_) => old_styles.push(prop),
                        Vproperty::Listener(_) => old_events.push(prop),
                        _ => old_attrs.push(prop),
                    }
                }

                let mut new_classes: Vec<&Vproperty> = Vec::new();
                let mut new_styles: Vec<&Vproperty> = Vec::new();
                let mut new_events: Vec<&Vproperty> = Vec::new();
                let mut new_attrs: Vec<&Vproperty> = Vec::new();
                for prop in new_el.props().iter() {
                    match prop {
                        Vproperty::Class(_) => new_classes.push(prop),
                        Vproperty::Style(_) => new_styles.push(prop),
                        Vproperty::Listener(_) => new_events.push(prop),
                        _ => new_attrs.push(prop),
                    }
                }

                let el = new_vdom.node.as_ref().unwrap().clone();
                patch_attrs(&el, &old_attrs, &new_attrs);
                patch_class(&el, &old_classes, &new_classes);
                patch_attrs(&el, &old_styles, &new_styles);
                patch_events(&el, &old_events, &new_events, host_component)
            };

            old_vdom.remove_listeners(remove_listener_names);
            new_vdom.set_listeners(new_listeners);
        }
        (VdomKind::Component(old_c), VdomKind::Component(new_c)) => {
            let old_c = Rc::clone(old_c);
            let new_state = new_c.borrow().state().clone();
            Component::set_state(&old_c, new_state);

            let old_node: Option<web_sys::Node> = old_c
                .borrow()
                .first_element()
                .map(|e| e.dyn_into().unwrap());
            new_vdom.node = old_node;
            new_vdom.kind = VdomKind::Component(Rc::clone(&old_c));
        }
        _ => {}
    }

    // web_sys::console::log_1(&"ENTERRRR".into());
    patch_children(old_vdom, new_vdom, host_component);
    // web_sys::console::log_1(&"EXITTTT".into());
}

fn patch_attrs(node: &web_sys::Node, old_attrs: &[&Vproperty], new_attrs: &[&Vproperty]) {
    let el: &web_sys::Element = node.dyn_ref().unwrap();
    let diff = diff_props(old_attrs, new_attrs);
    for removed in diff.removed.iter().chain(diff.updated.iter()) {
        set_attribute(el, removed.clear_value());
    }
    for added in diff.added.into_iter().chain(diff.updated.into_iter()) {
        set_attribute(el, added);
    }
}

fn patch_class(node: &web_sys::Node, old_attrs: &[&Vproperty], new_attrs: &[&Vproperty]) {
    let el: &web_sys::Element = node.dyn_ref().unwrap();
    let diff = diff_props(old_attrs, new_attrs);
    if let Some(removed) = diff.removed.iter().chain(diff.updated.iter()).next() {
        set_attribute(el, removed.clear_value());
    }
    for added in diff.added.into_iter().chain(diff.updated.into_iter()) {
        set_attribute(el, added);
    }
}

fn patch_events(
    el: &web_sys::Node,
    old_events: &[&Vproperty],
    new_events: &[&Vproperty],
    host_component: &Rc<RefCell<Component>>,
) -> (Vec<Listener>, HashSet<String>) {
    let diff = diff_props(old_events, new_events);
    let mut remove_listener_names = HashSet::new();
    //for event in diff.removed.iter().chain(diff.updated.iter()) {
    for event in diff.removed.iter() {
        let event_name = match event {
            Vproperty::Listener((name, _)) => name,
            _ => unreachable!(),
        };
        remove_listener_names.insert(event_name.to_string());
    }

    let mut listeners = HashMap::new();
    // for event in diff.added.into_iter().chain(diff.updated.into_iter()) {
    for event in diff.added.into_iter() {
        match event {
            Vproperty::Listener((event_name, handler)) => {
                listeners.insert(event_name, handler);
            }
            _ => unreachable!(),
        }
    }
    let new_listeners = add_event_listeners(el, listeners, host_component);
    // web_sys::console::log_1(
    //     &format!(
    //         "PATCH_EVENTS: EL: {:?} NEW: {:?}, REMOVE: {:?}",
    //         el.node_name(),
    //         new_listeners.iter().map(|l| l.name()).collect::<Vec<_>>(),
    //         remove_listener_names,
    //     )
    //     .into(),
    // );
    (new_listeners, remove_listener_names)
}

fn patch_children(
    old_vdom: &mut Vdom,
    new_vdom: &mut Vdom,
    host_component: &Rc<RefCell<Component>>,
) {
    let parent_node = old_vdom.node.as_ref().unwrap().clone();
    let old_children = old_vdom.extract_children();
    let new_children = new_vdom.extract_children();
    // web_sys::console::log_1(&format!("PATCH_CHILD: old: {old_children:?}, new: {new_children:?}").into());
    let mut diff_seq = diff_vdoms(&old_children, &new_children);
    //web_sys::console::log_1(&format!("PATCH_CHILD: diff: {diff_seq:?}").into());

    let offset = host_component.borrow().offset();
    for operation in diff_seq.iter_mut() {
        let idx = operation.idx;
        match operation.kind {
            OperationKind::Add => {
                mount_dom(
                    &mut operation.item.borrow_mut(),
                    &parent_node,
                    Some(idx as u32 + offset),
                    host_component,
                );
            }
            OperationKind::Remove => {
                destroy_dom(&mut operation.item.borrow_mut());
            }
            OperationKind::Move => {
                let orig_idx = operation.original_index.unwrap() as usize;
                let old_child = &old_children[orig_idx];
                let new_child = &new_children[idx];
                let el = old_child.borrow_mut().node.as_ref().unwrap().clone();
                let el_at_target_idx = parent_node.child_nodes().get(idx as u32 + offset);

                parent_node
                    .insert_before(&el, el_at_target_idx.as_ref())
                    .unwrap();
                patch_dom(
                    &mut old_child.borrow_mut(),
                    &mut new_child.borrow_mut(),
                    &parent_node,
                    host_component,
                );
            }
            OperationKind::Noop => {
                let orig_idx = operation.original_index.unwrap() as usize;
                let old_child = &old_children[orig_idx];
                let new_child = &new_children[idx];
                patch_dom(
                    &mut old_child.borrow_mut(),
                    &mut new_child.borrow_mut(),
                    &parent_node,
                    host_component,
                );
            }
        }
    }
}

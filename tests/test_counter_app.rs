mod common;
use common::*;

use wasm_bindgen_test::*;
wasm_bindgen_test_configure!(run_in_browser);

use std::{cell::RefCell, collections::HashMap, rc::Rc};
use toy_fe_fwk::*;
use web_sys::wasm_bindgen::JsCast;

#[derive(Debug, Clone, PartialEq, Default)]
struct RootState {
    count: i32,
}

impl Cstate for RootState {
    fn as_any(&self) -> &dyn std::any::Any {
        self
    }

    fn clone(&self) -> Box<dyn Cstate> {
        Box::new(std::clone::Clone::clone(self))
    }

    fn equals(&self, other: &dyn Cstate) -> bool {
        other
            .as_any()
            .downcast_ref::<Self>()
            .map(|o| o == self)
            .unwrap_or(false)
    }
}

fn root_plus(component: Rc<RefCell<Component>>, _payload: Payload) {
    let new_state = {
        let component_rc = component.borrow();
        let old = component_rc
            .state()
            .as_any()
            .downcast_ref::<RootState>()
            .unwrap();
        Box::new(RootState {
            count: old.count + 1,
        })
    };
    Component::set_state(&component, new_state);
}

fn root_minus(component: Rc<RefCell<Component>>, _payload: Payload) {
    let new_state = {
        let component_rc = component.borrow();
        let old = component_rc
            .state()
            .as_any()
            .downcast_ref::<RootState>()
            .unwrap();
        Box::new(RootState {
            count: old.count - 1,
        })
    };
    Component::set_state(&component, new_state);
}

fn root_component() -> Rc<RefCell<Component>> {
    let id = "root";
    let mut event_handlers: HashMap<String, Chandler> = HashMap::new();
    event_handlers.insert(format!("{id}_plus"), Rc::new(root_plus));
    event_handlers.insert(format!("{id}_minus"), Rc::new(root_minus));

    let view_fn = Box::new(move |state: &dyn Cstate| -> Vdom {
        let state = state.as_any().downcast_ref::<RootState>().unwrap();
        Vdom::fragment(vec![
            Vdom::component(minus_component(id)),
            Vdom::element(
                "span",
                vec![],
                vec![Vdom::text(&format!("{}", state.count))],
            ),
            Vdom::component(plus_component(id)),
        ])
    });

    Rc::new(RefCell::new(Component::new(
        id,
        None,
        Box::<RootState>::default(),
        event_handlers,
        view_fn,
    )))
}

#[derive(Debug, Clone, PartialEq, Default)]
struct EmptyState {}

impl Cstate for EmptyState {
    fn as_any(&self) -> &dyn std::any::Any {
        self
    }

    fn clone(&self) -> Box<dyn Cstate> {
        Box::new(std::clone::Clone::clone(self))
    }

    fn equals(&self, other: &dyn Cstate) -> bool {
        other
            .as_any()
            .downcast_ref::<Self>()
            .map(|o| o == self)
            .unwrap_or(false)
    }
}

fn plus_plus(component: Rc<RefCell<Component>>, payload: Payload) {
    bubbleup_event("plus", component, payload);
}

fn listener_plus(component: Rc<RefCell<Component>>, _event: web_sys::Event) {
    let id = component.borrow().id().to_string();
    let (command_handler, _after_handlers) =
        HANDLERS.with_borrow_mut(|h| h.get_handlers(&format!("{id}_plus")));
    command_handler(Payload::Empty);
}

fn plus_component(parent: &str) -> Rc<RefCell<Component>> {
    let id = "plus";
    let mut event_handlers: HashMap<String, Chandler> = HashMap::new();
    event_handlers.insert(format!("{id}_plus"), Rc::new(plus_plus));

    let view_fn = Box::new(move |_state: &dyn Cstate| -> Vdom {
        Vdom::fragment(vec![Vdom::element(
            "button",
            vec![
                Vproperty::Listener(("click".to_string(), Rc::new(listener_plus))),
                Vproperty::Data(("data-qa".to_string(), Some("plus-btn".to_string()))),
            ],
            vec![Vdom::text("+")],
        )])
    });

    Rc::new(RefCell::new(Component::new(
        id,
        Some(parent.to_string()),
        Box::<EmptyState>::default(),
        event_handlers,
        view_fn,
    )))
}

fn minus_minus(component: Rc<RefCell<Component>>, payload: Payload) {
    bubbleup_event("minus", component, payload);
}

fn listener_minus(component: Rc<RefCell<Component>>, _event: web_sys::Event) {
    let id = component.borrow().id().to_string();
    let (command_handler, _after_handlers) =
        HANDLERS.with_borrow_mut(|h| h.get_handlers(&format!("{id}_minus")));
    command_handler(Payload::Empty);
}

fn minus_component(parent: &str) -> Rc<RefCell<Component>> {
    let id = "minus";
    let mut event_handlers: HashMap<String, Chandler> = HashMap::new();
    event_handlers.insert(format!("{id}_minus"), Rc::new(minus_minus));

    let view_fn = Box::new(move |_state: &dyn Cstate| -> Vdom {
        Vdom::fragment(vec![Vdom::element(
            "button",
            vec![
                Vproperty::Listener(("click".to_string(), Rc::new(listener_minus))),
                Vproperty::Data(("data-qa".to_string(), Some("minus-btn".to_string()))),
            ],
            vec![Vdom::text("-")],
        )])
    });

    Rc::new(RefCell::new(Component::new(
        id,
        Some(parent.to_string()),
        Box::<EmptyState>::default(),
        event_handlers,
        view_fn,
    )))
}

#[wasm_bindgen_test]
fn test_app() {
    let root = root_component();
    mount_app(root);

    let expected = "\
          <button data-qa=\"minus-btn\">-</button>\
          <span>0</span>\
          <button data-qa=\"plus-btn\">+</button>\
       ";
    assert_eq!(parent(), expected);

    let inc_btn = find_el("[data-qa=\"plus-btn\"]");
    inc_btn.click();

    let expected = "\
          <button data-qa=\"minus-btn\">-</button>\
          <span>1</span>\
          <button data-qa=\"plus-btn\">+</button>\
       ";
    assert_eq!(parent(), expected);

    let dec_btn = find_el("[data-qa=\"minus-btn\"]");
    dec_btn.click();

    let expected = "\
          <button data-qa=\"minus-btn\">-</button>\
          <span>0</span>\
          <button data-qa=\"plus-btn\">+</button>\
       ";
    assert_eq!(parent(), expected);
}

mod common;
use common::*;

use wasm_bindgen_test::*;
wasm_bindgen_test_configure!(run_in_browser);
use std::{cell::RefCell, rc::Rc};
use toy_fe_fwk::*;
use web_sys::wasm_bindgen::JsCast;

fn patch(old_vdom: Vdom, new_vdom: Vdom) -> (Vdom, Vdom) {
    let mut new_vdom = new_vdom;
    let component = test_component(old_vdom);
    mount_app(Rc::clone(&component));
    let (mut old_vdom, host_el) = {
        let mut component_rc = component.borrow_mut();
        let old_vdom = component_rc.vdom.as_mut().unwrap().clone();
        let host_el = component_rc.host_el.as_ref().unwrap().clone();
        (old_vdom, host_el)
    };

    patch_dom(&mut old_vdom, &mut new_vdom, &host_el, &component);
    (old_vdom, new_vdom)
}

#[wasm_bindgen_test]
fn no_change() {
    let old_vdom = Vdom::element("div", vec![], vec![Vdom::text("hello")]);
    let new_vdom = Vdom::element("div", vec![], vec![Vdom::text("hello")]);
    let (old, new) = patch(old_vdom, new_vdom);

    assert_eq!(parent(), "<div>hello</div>");
    assert_eq!(new, old);
}

#[wasm_bindgen_test]
fn change_the_root_node() {
    let old_vdom = Vdom::element("div", vec![], vec![Vdom::text("hello")]);
    let new_vdom = Vdom::element("span", vec![], vec![Vdom::text("hello")]);
    let (old, new) = patch(old_vdom, new_vdom);

    assert_eq!(parent(), "<span>hello</span>");
    assert!(new.node.is_some());
    assert!(old.node.is_none());
}

#[wasm_bindgen_test]
fn patch_text() {
    let old = Vdom::text("foo");
    let new = Vdom::text("bar");

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "bar");
}

#[wasm_bindgen_test]
fn nested_fragments_add_child() {
    let old = Vdom::fragment(vec![Vdom::fragment(vec![Vdom::text("foo")])]);
    let new = Vdom::fragment(vec![
        Vdom::fragment(vec![Vdom::text("foo"), Vdom::text("bar")]),
        Vdom::element("p", vec![], vec![Vdom::text("baz")]),
    ]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "foobar<p>baz</p>");
}

#[wasm_bindgen_test]
fn nested_fragments_remove_child() {
    let old = Vdom::fragment(vec![
        Vdom::fragment(vec![Vdom::text("x")]),
        Vdom::text("a"),
        Vdom::fragment(vec![
            Vdom::text("b"),
            Vdom::fragment(vec![Vdom::text("y"), Vdom::text("c")]),
        ]),
    ]);
    let new = Vdom::fragment(vec![
        Vdom::text("a"),
        Vdom::fragment(vec![Vdom::text("b"), Vdom::text("c")]),
    ]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "abc");
}

#[wasm_bindgen_test]
fn nested_fragments_add_child_at_index() {
    let old = Vdom::fragment(vec![
        Vdom::text("a"),
        Vdom::fragment(vec![Vdom::text("b"), Vdom::text("c")]),
    ]);
    let new = Vdom::fragment(vec![
        Vdom::fragment(vec![Vdom::text("x")]),
        Vdom::text("a"),
        Vdom::fragment(vec![Vdom::text("b"), Vdom::text("y"), Vdom::text("c")]),
    ]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "xabyc");
}

#[wasm_bindgen_test]
fn nested_fragments_move_child() {
    let old = Vdom::fragment(vec![
        Vdom::text("a"),
        Vdom::fragment(vec![Vdom::text("b"), Vdom::text("c")]),
    ]);
    let new = Vdom::fragment(vec![
        Vdom::fragment(vec![Vdom::text("b")]),
        Vdom::text("a"),
        Vdom::fragment(vec![Vdom::text("c")]),
    ]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "bac");
}

#[wasm_bindgen_test]
fn attribute_add() {
    let old = Vdom::element("div", vec![], vec![]);
    let new = Vdom::element("div", vec![Vproperty::Id(Some("foo".to_string()))], vec![]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div id=\"foo\"></div>");
}

#[wasm_bindgen_test]
fn attribute_remove() {
    let old = Vdom::element("div", vec![Vproperty::Id(Some("foo".to_string()))], vec![]);
    let new = Vdom::element("div", vec![], vec![]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div id=\"\"></div>");
}

#[wasm_bindgen_test]
fn attribute_update() {
    let old = Vdom::element("div", vec![Vproperty::Id(Some("foo".to_string()))], vec![]);
    let new = Vdom::element("div", vec![Vproperty::Id(Some("bar".to_string()))], vec![]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div id=\"bar\"></div>");
}

#[wasm_bindgen_test]
fn class_add() {
    let old = Vdom::element("div", vec![], vec![]);
    let new = Vdom::element(
        "div",
        vec![Vproperty::Class(Some("foo".to_string()))],
        vec![],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div class=\"foo\"></div>");
}

#[wasm_bindgen_test]
fn class_remove() {
    let old = Vdom::element(
        "div",
        vec![Vproperty::Class(Some("foo".to_string()))],
        vec![],
    );
    let new = Vdom::element("div", vec![], vec![]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div class=\"\"></div>");
}

#[wasm_bindgen_test]
fn class_update() {
    let old = Vdom::element(
        "div",
        vec![Vproperty::Class(Some("foo".to_string()))],
        vec![],
    );
    let new = Vdom::element(
        "div",
        vec![Vproperty::Class(Some("bar".to_string()))],
        vec![],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div class=\"bar\"></div>");
}

#[wasm_bindgen_test]
fn class_from_multiple_to_one_1() {
    let old = Vdom::element(
        "div",
        vec![Vproperty::Class(Some("foo bar".to_string()))],
        vec![],
    );
    let new = Vdom::element(
        "div",
        vec![Vproperty::Class(Some("foo".to_string()))],
        vec![],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div class=\"foo\"></div>");
}

#[wasm_bindgen_test]
fn class_from_one_to_multiple_1() {
    let old = Vdom::element(
        "div",
        vec![Vproperty::Class(Some("foo".to_string()))],
        vec![],
    );
    let new = Vdom::element(
        "div",
        vec![Vproperty::Class(Some("foo bar".to_string()))],
        vec![],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div class=\"foo bar\"></div>");
}

#[wasm_bindgen_test]
fn class_from_multiple_to_one_2() {
    let old = Vdom::element(
        "div",
        vec![
            Vproperty::Class(Some("foo".to_string())),
            Vproperty::Class(Some("bar".to_string())),
        ],
        vec![],
    );
    let new = Vdom::element(
        "div",
        vec![Vproperty::Class(Some("baz".to_string()))],
        vec![],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div class=\"baz\"></div>");
}

#[wasm_bindgen_test]
fn class_from_one_to_multiple_2() {
    let old = Vdom::element(
        "div",
        vec![Vproperty::Class(Some("foo".to_string()))],
        vec![],
    );
    let new = Vdom::element(
        "div",
        vec![
            Vproperty::Class(Some("bar".to_string())),
            Vproperty::Class(Some("baz".to_string())),
        ],
        vec![],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div class=\"bar baz\"></div>");
}

#[wasm_bindgen_test]
fn style_add() {
    let old = Vdom::element("div", vec![], vec![]);
    let new = Vdom::element(
        "div",
        vec![Vproperty::Style((
            "color".to_string(),
            Some("red".to_string()),
        ))],
        vec![],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div style=\"color: red;\"></div>");
}

#[wasm_bindgen_test]
fn style_remove() {
    let old = Vdom::element(
        "div",
        vec![Vproperty::Style((
            "color".to_string(),
            Some("red".to_string()),
        ))],
        vec![],
    );
    let new = Vdom::element("div", vec![], vec![]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div style=\"\"></div>");
}

#[wasm_bindgen_test]
fn style_update() {
    let old = Vdom::element(
        "div",
        vec![Vproperty::Style((
            "color".to_string(),
            Some("red".to_string()),
        ))],
        vec![],
    );
    let new = Vdom::element(
        "div",
        vec![Vproperty::Style((
            "color".to_string(),
            Some("blue".to_string()),
        ))],
        vec![],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div style=\"color: blue;\"></div>");
}

#[wasm_bindgen_test]
fn text_add_at_end() {
    let old = Vdom::element("div", vec![], vec![Vdom::text("A")]);
    let new = Vdom::element("div", vec![], vec![Vdom::text("A"), Vdom::text("B")]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div>AB</div>");
}

#[wasm_bindgen_test]
fn text_add_at_beginning() {
    let old = Vdom::element("div", vec![], vec![Vdom::text("B")]);
    let new = Vdom::element("div", vec![], vec![Vdom::text("A"), Vdom::text("B")]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div>AB</div>");
}

#[wasm_bindgen_test]
fn text_add_in_middle() {
    let old = Vdom::element("div", vec![], vec![Vdom::text("A"), Vdom::text("B")]);
    let new = Vdom::element(
        "div",
        vec![],
        vec![Vdom::text("A"), Vdom::text("B"), Vdom::text("C")],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div>ABC</div>");
}

#[wasm_bindgen_test]
fn text_remove_from_end() {
    let old = Vdom::element("div", vec![], vec![Vdom::text("A"), Vdom::text("B")]);
    let new = Vdom::element("div", vec![], vec![Vdom::text("A")]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div>A</div>");
}

#[wasm_bindgen_test]
fn text_remove_from_beggining() {
    let old = Vdom::element("div", vec![], vec![Vdom::text("A"), Vdom::text("B")]);
    let new = Vdom::element("div", vec![], vec![Vdom::text("B")]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div>B</div>");
}

#[wasm_bindgen_test]
fn text_remove_from_middle() {
    let old = Vdom::element(
        "div",
        vec![],
        vec![Vdom::text("A"), Vdom::text("B"), Vdom::text("C")],
    );
    let new = Vdom::element("div", vec![], vec![Vdom::text("A"), Vdom::text("C")]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div>AC</div>");
}

#[wasm_bindgen_test]
fn text_update() {
    let old = Vdom::element("div", vec![], vec![Vdom::text("A")]);
    let new = Vdom::element("div", vec![], vec![Vdom::text("B")]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div>B</div>");
}

#[wasm_bindgen_test]
fn text_move_around() {
    let old = Vdom::element(
        "div",
        vec![],
        vec![Vdom::text("A"), Vdom::text("B"), Vdom::text("C")],
    );
    let new = Vdom::element(
        "div",
        vec![],
        vec![Vdom::text("C"), Vdom::text("A"), Vdom::text("B")],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div>CAB</div>");
}

#[wasm_bindgen_test]
fn text_update_recursively() {
    let old = Vdom::fragment(vec![
        Vdom::element("p", vec![], vec![Vdom::text("A")]),
        Vdom::element("span", vec![], vec![Vdom::text("B")]),
        Vdom::element("div", vec![], vec![Vdom::text("C")]),
    ]);
    let new = Vdom::fragment(vec![
        Vdom::element("div", vec![], vec![Vdom::text("C")]),
        Vdom::element(
            "span",
            vec![Vproperty::Id(Some("b".to_string()))],
            vec![Vdom::text("B")],
        ),
    ]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div>C</div><span id=\"b\">B</span>");
}

#[wasm_bindgen_test]
fn element_add_at_end() {
    let old = Vdom::element(
        "div",
        vec![],
        vec![Vdom::element("span", vec![], vec![Vdom::text("A")])],
    );
    let new = Vdom::element(
        "div",
        vec![],
        vec![
            Vdom::element("span", vec![], vec![Vdom::text("A")]),
            Vdom::element(
                "span",
                vec![Vproperty::Id(Some("b".to_string()))],
                vec![Vdom::text("B")],
            ),
        ],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div><span>A</span><span id=\"b\">B</span></div>");
}

#[wasm_bindgen_test]
fn element_add_at_beginning() {
    let old = Vdom::element(
        "div",
        vec![],
        vec![Vdom::element("span", vec![], vec![Vdom::text("B")])],
    );
    let new = Vdom::element(
        "div",
        vec![],
        vec![
            Vdom::element(
                "span",
                vec![Vproperty::Id(Some("a".to_string()))],
                vec![Vdom::text("A")],
            ),
            Vdom::element("span", vec![], vec![Vdom::text("B")]),
        ],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div><span id=\"a\">A</span><span>B</span></div>");
}

#[wasm_bindgen_test]
fn element_add_in_middle() {
    let old = Vdom::element(
        "div",
        vec![],
        vec![
            Vdom::element("span", vec![], vec![Vdom::text("A")]),
            Vdom::element("span", vec![], vec![Vdom::text("C")]),
        ],
    );
    let new = Vdom::element(
        "div",
        vec![],
        vec![
            Vdom::element("span", vec![], vec![Vdom::text("A")]),
            Vdom::element(
                "span",
                vec![Vproperty::Id(Some("b".to_string()))],
                vec![Vdom::text("B")],
            ),
            Vdom::element("span", vec![], vec![Vdom::text("C")]),
        ],
    );

    let (old, new) = patch(old, new);

    assert_eq!(
        parent(),
        "<div><span>A</span><span id=\"b\">B</span><span>C</span></div>"
    );
}

#[wasm_bindgen_test]
fn element_remove_from_end() {
    let old = Vdom::element(
        "div",
        vec![],
        vec![
            Vdom::element("span", vec![], vec![Vdom::text("A")]),
            Vdom::element("span", vec![], vec![Vdom::text("B")]),
        ],
    );
    let new = Vdom::element(
        "div",
        vec![],
        vec![Vdom::element("span", vec![], vec![Vdom::text("A")])],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div><span>A</span></div>");
}

#[wasm_bindgen_test]
fn element_remove_from_beggining() {
    let old = Vdom::element(
        "div",
        vec![],
        vec![
            Vdom::element("span", vec![], vec![Vdom::text("A")]),
            Vdom::element("span", vec![], vec![Vdom::text("B")]),
        ],
    );
    let new = Vdom::element(
        "div",
        vec![],
        vec![Vdom::element("span", vec![], vec![Vdom::text("B")])],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div><span>B</span></div>");
}

#[wasm_bindgen_test]
fn element_remove_from_middle() {
    let old = Vdom::element(
        "div",
        vec![],
        vec![
            Vdom::element("span", vec![], vec![Vdom::text("A")]),
            Vdom::element("span", vec![], vec![Vdom::text("B")]),
            Vdom::element("span", vec![], vec![Vdom::text("C")]),
        ],
    );
    let new = Vdom::element(
        "div",
        vec![],
        vec![
            Vdom::element("span", vec![], vec![Vdom::text("A")]),
            Vdom::element("span", vec![], vec![Vdom::text("C")]),
        ],
    );

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<div><span>A</span><span>C</span></div>");
}

#[wasm_bindgen_test]
fn element_move_around() {
    let old = Vdom::element(
        "div",
        vec![],
        vec![
            Vdom::element("span", vec![], vec![Vdom::text("A")]),
            Vdom::element("span", vec![], vec![Vdom::text("B")]),
            Vdom::element("span", vec![], vec![Vdom::text("C")]),
        ],
    );
    let new = Vdom::element(
        "div",
        vec![],
        vec![
            Vdom::element("span", vec![], vec![Vdom::text("C")]),
            Vdom::element("span", vec![], vec![Vdom::text("A")]),
            Vdom::element("span", vec![], vec![Vdom::text("B")]),
        ],
    );

    let (old, new) = patch(old, new);

    assert_eq!(
        parent(),
        "<div><span>C</span><span>A</span><span>B</span></div>"
    );
}

#[wasm_bindgen_test]
fn fragment_add_element_at_end() {
    let old = Vdom::fragment(vec![Vdom::element("span", vec![], vec![Vdom::text("A")])]);
    let new = Vdom::fragment(vec![
        Vdom::element("span", vec![], vec![Vdom::text("A")]),
        Vdom::element(
            "span",
            vec![Vproperty::Id(Some("b".to_string()))],
            vec![Vdom::text("B")],
        ),
    ]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<span>A</span><span id=\"b\">B</span>");
}

#[wasm_bindgen_test]
fn fragment_add_element_at_beginning() {
    let old = Vdom::fragment(vec![Vdom::element("span", vec![], vec![Vdom::text("B")])]);
    let new = Vdom::fragment(vec![
        Vdom::element(
            "span",
            vec![Vproperty::Id(Some("a".to_string()))],
            vec![Vdom::text("A")],
        ),
        Vdom::element("span", vec![], vec![Vdom::text("B")]),
    ]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<span id=\"a\">A</span><span>B</span>");
}

#[wasm_bindgen_test]
fn fragment_add_element_in_middle() {
    let old = Vdom::fragment(vec![
        Vdom::element("span", vec![], vec![Vdom::text("A")]),
        Vdom::element("span", vec![], vec![Vdom::text("C")]),
    ]);
    let new = Vdom::fragment(vec![
        Vdom::element("span", vec![], vec![Vdom::text("A")]),
        Vdom::element(
            "span",
            vec![Vproperty::Id(Some("b".to_string()))],
            vec![Vdom::text("B")],
        ),
        Vdom::element("span", vec![], vec![Vdom::text("C")]),
    ]);

    let (old, new) = patch(old, new);

    assert_eq!(
        parent(),
        "<span>A</span><span id=\"b\">B</span><span>C</span>"
    );
}

#[wasm_bindgen_test]
fn fragment_remove_element_from_end() {
    let old = Vdom::fragment(vec![
        Vdom::element("span", vec![], vec![Vdom::text("A")]),
        Vdom::element("span", vec![], vec![Vdom::text("B")]),
    ]);
    let new = Vdom::fragment(vec![Vdom::element("span", vec![], vec![Vdom::text("A")])]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<span>A</span>");
}

#[wasm_bindgen_test]
fn fragment_remove_element_from_beggining() {
    let old = Vdom::fragment(vec![
        Vdom::element("span", vec![], vec![Vdom::text("A")]),
        Vdom::element("span", vec![], vec![Vdom::text("B")]),
    ]);
    let new = Vdom::fragment(vec![Vdom::element("span", vec![], vec![Vdom::text("B")])]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<span>B</span>");
}

#[wasm_bindgen_test]
fn fragment_remove_element_from_middle() {
    let old = Vdom::fragment(vec![
        Vdom::element("span", vec![], vec![Vdom::text("A")]),
        Vdom::element("span", vec![], vec![Vdom::text("B")]),
        Vdom::element("span", vec![], vec![Vdom::text("C")]),
    ]);
    let new = Vdom::fragment(vec![
        Vdom::element("span", vec![], vec![Vdom::text("A")]),
        Vdom::element("span", vec![], vec![Vdom::text("C")]),
    ]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<span>A</span><span>C</span>");
}

#[wasm_bindgen_test]
fn fragment_append() {
    let old = Vdom::fragment(vec![Vdom::element("span", vec![], vec![Vdom::text("A")])]);
    let new = Vdom::fragment(vec![
        Vdom::element("span", vec![], vec![Vdom::text("A")]),
        Vdom::fragment(vec![Vdom::element("span", vec![], vec![Vdom::text("B")])]),
    ]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<span>A</span><span>B</span>");
}

#[wasm_bindgen_test]
fn fragment_move_around() {
    let old = Vdom::fragment(vec![
        Vdom::element("span", vec![], vec![Vdom::text("A")]),
        Vdom::element("span", vec![], vec![Vdom::text("B")]),
        Vdom::element("span", vec![], vec![Vdom::text("C")]),
    ]);
    let new = Vdom::fragment(vec![
        Vdom::element("span", vec![], vec![Vdom::text("C")]),
        Vdom::element("span", vec![], vec![Vdom::text("A")]),
        Vdom::element("span", vec![], vec![Vdom::text("B")]),
    ]);

    let (old, new) = patch(old, new);

    assert_eq!(parent(), "<span>C</span><span>A</span><span>B</span>");
}

mod common;
use common::TestContext;

use wasm_bindgen_test::*;
wasm_bindgen_test_configure!(run_in_browser);
use toy_fe_fwk::*;
use web_sys::wasm_bindgen::JsCast;

#[wasm_bindgen_test]
fn set_class_from_string() {
    let tc = TestContext::new();
    set_attributes(
        &tc.parent_el(),
        vec![Vproperty::Class(Some("foo bar".to_string()))],
    );
    assert_eq!("foo bar", tc.class_name());
}

#[wasm_bindgen_test]
fn set_class_from_array() {
    let tc = TestContext::new();
    set_attributes(
        &tc.parent_el(),
        vec![
            Vproperty::Class(Some("foo".to_string())),
            Vproperty::Class(Some("bar".to_string())),
        ],
    );
    assert_eq!("foo bar", tc.class_name());
}

#[wasm_bindgen_test]
fn update_class() {
    let tc = TestContext::new();
    set_attributes(
        &tc.parent_el(),
        vec![Vproperty::Class(Some("foo".to_string()))],
    );
    set_attributes(&tc.parent_el(), vec![Vproperty::Class(None)]);
    set_attributes(
        &tc.parent_el(),
        vec![Vproperty::Class(Some("bar baz".to_string()))],
    );
    assert_eq!("bar baz", tc.class_name());
}

#[wasm_bindgen_test]
fn set_style() {
    let tc = TestContext::new();
    set_attributes(
        &tc.parent_el(),
        vec![
            Vproperty::Style(("color".to_string(), Some("red".to_string()))),
            Vproperty::Style(("background-color".to_string(), Some("blue".to_string()))),
        ],
    );
    assert_eq!(
        "red",
        tc.parent()
            .dyn_ref::<web_sys::HtmlElement>()
            .unwrap()
            .style()
            .get_property_value("color")
            .unwrap()
    );
    assert_eq!(
        "blue",
        tc.parent()
            .dyn_ref::<web_sys::HtmlElement>()
            .unwrap()
            .style()
            .get_property_value("background-color")
            .unwrap()
    );
}

#[wasm_bindgen_test]
fn set_attribute_data() {
    let tc = TestContext::new();
    set_attributes(
        &tc.parent_el(),
        vec![
            Vproperty::Data(("data-my-attr".to_string(), Some("foo".to_string()))),
            Vproperty::Data(("hidden".to_string(), Some("true".to_string()))),
            Vproperty::Data(("hidden".to_string(), None)),
        ],
    );
    assert_eq!("foo", tc.parent_el().get_attribute("data-my-attr").unwrap());
    assert!(tc.parent_el().get_attribute("hidden").is_none());
}

#[wasm_bindgen_test]
fn set_attribute_on_input() {
    let input: web_sys::HtmlInputElement = create_element("input").unwrap().dyn_into().unwrap();

    set_attributes(
        &input,
        vec![
            Vproperty::Type(Some("text".to_string())),
            Vproperty::Value(Some("foo bar".to_string())),
            Vproperty::Disabled(Some(true)),
            Vproperty::Size(Some(1)),
        ],
    );
    assert_eq!("text", input.type_());
    assert_eq!("foo bar", input.value());
    assert_eq!(true, input.disabled());
    assert_eq!(1, input.size());

    set_attributes(
        &input,
        vec![
            Vproperty::Type(None),
            Vproperty::Value(None),
            Vproperty::Disabled(None),
            Vproperty::Size(None),
        ],
    );
    assert_eq!("text", input.type_());
    assert_eq!("", input.value());
    assert_eq!(false, input.disabled());
    assert_eq!(20, input.size());

    set_attributes(
        &input,
        vec![
            Vproperty::Type(Some("radio".to_string())),
            Vproperty::Checked(Some(true)),
        ],
    );
    assert_eq!("radio", input.type_());
    assert_eq!(true, input.checked());

    set_attributes(
        &input,
        vec![Vproperty::Type(None), Vproperty::Checked(None)],
    );
    assert_eq!("text", input.type_());
    assert_eq!(false, input.checked());
}

#[wasm_bindgen_test]
fn set_attribute_on_select() {
    let select: web_sys::HtmlSelectElement = create_element("select").unwrap().dyn_into().unwrap();
    let option_foo: web_sys::HtmlOptionElement =
        create_element("option").unwrap().dyn_into().unwrap();
    option_foo.set_value("foo");
    select.add_with_html_option_element(&option_foo).unwrap();
    let option_bar: web_sys::HtmlOptionElement =
        create_element("option").unwrap().dyn_into().unwrap();
    option_foo.set_value("bar");
    select.add_with_html_option_element(&option_bar).unwrap();

    set_attributes(&select, vec![Vproperty::Selected(Some(1))]);
    assert_eq!(1, select.selected_index());
}

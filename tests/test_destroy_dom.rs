mod common;
use common::*;

use wasm_bindgen_test::*;
wasm_bindgen_test_configure!(run_in_browser);
use std::{cell::RefCell, rc::Rc};
use toy_fe_fwk::*;
use web_sys::wasm_bindgen::JsCast;

fn all_destroyed(vdom: &Vdom) -> bool {
    if vdom.node.is_some() {
        return false;
    }

    match &vdom.kind {
        VdomKind::Text(_) => true,
        VdomKind::Element(vel) => vel.children().iter().all(|c| all_destroyed(&*c.borrow())),
        VdomKind::Fragment(children) => children.iter().all(|c| all_destroyed(&*c.borrow())),
        VdomKind::Component(c) => !c.borrow().is_mounted,
    }
}

#[wasm_bindgen_test]
fn test_unmount_app() {
    let vdom = Vdom::text("hello");
    let component = test_component(vdom);
    mount_app(Rc::clone(&component));
    assert_eq!("hello", parent());
    unmount_app(component);
    assert_eq!("", parent());
}

#[wasm_bindgen_test]
fn destroy_text() {
    let vdom = Vdom::text("hello");
    let component = test_component(vdom);
    mount_app(Rc::clone(&component));
    assert_eq!("hello", parent());

    let mut component_rc = component.borrow_mut();
    let vdom = component_rc.vdom.as_mut().unwrap();
    destroy_dom(vdom);
    assert_eq!("", parent());
    assert!(all_destroyed(vdom));
}

#[wasm_bindgen_test]
fn destroy_element() {
    let vdom = Vdom::element("div", vec![], vec![Vdom::text("hello")]);
    let component = test_component(vdom);
    mount_app(Rc::clone(&component));
    assert_eq!("<div>hello</div>", parent());

    let mut component_rc = component.borrow_mut();
    let vdom = component_rc.vdom.as_mut().unwrap();
    destroy_dom(vdom);
    assert_eq!("", parent());
    assert!(all_destroyed(vdom));
}

#[wasm_bindgen_test]
fn destroy_fragment_recursively() {
    let vdom = Vdom::fragment(vec![
        Vdom::element("p", vec![], vec![Vdom::text("foo")]),
        Vdom::fragment(vec![
            Vdom::element("p", vec![], vec![Vdom::text("bar")]),
            Vdom::element("p", vec![], vec![Vdom::text("baz")]),
        ]),
    ]);
    let component = test_component(vdom);
    mount_app(Rc::clone(&component));
    assert_eq!("<p>foo</p><p>bar</p><p>baz</p>", parent());

    let mut component_rc = component.borrow_mut();
    let vdom = component_rc.vdom.as_mut().unwrap();
    destroy_dom(vdom);
    assert_eq!("", parent());
    assert!(all_destroyed(vdom));
}

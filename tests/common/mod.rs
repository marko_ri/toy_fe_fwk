use std::{cell::RefCell, collections::HashMap, rc::Rc};
use toy_fe_fwk::*;
use web_sys::wasm_bindgen::JsCast;

pub struct TestContext {
    body: web_sys::HtmlElement,
}

impl TestContext {
    pub fn new() -> Self {
        let body = web_sys::window()
            .unwrap()
            .document()
            .unwrap()
            .body()
            .unwrap();
        let parent = create_element("div").unwrap();
        body.append_child(&parent).unwrap();
        Self { body }
    }

    pub fn parent(&self) -> web_sys::Node {
        self.body.last_child().unwrap()
    }

    pub fn parent_el(&self) -> web_sys::Element {
        self.parent().dyn_into().unwrap()
    }

    pub fn class_name(&self) -> String {
        self.parent_el().class_name()
    }
}

#[derive(Debug, Clone, PartialEq, Default)]
pub struct EmptyState {}

impl Cstate for EmptyState {
    fn as_any(&self) -> &dyn std::any::Any {
        self
    }

    fn clone(&self) -> Box<dyn Cstate> {
        Box::new(std::clone::Clone::clone(self))
    }

    fn equals(&self, other: &dyn Cstate) -> bool {
        other
            .as_any()
            .downcast_ref::<Self>()
            .map(|o| o == self)
            .unwrap_or(false)
    }
}

pub fn test_component(vdom: Vdom) -> Rc<RefCell<Component>> {
    let id = "root";

    let view_fn = Box::new(move |_state: &dyn Cstate| -> Vdom { vdom.clone() });
    Rc::new(RefCell::new(Component::new(
        id,
        None,
        Box::<EmptyState>::default(),
        HashMap::new(),
        view_fn,
    )))
}

pub fn parent() -> String {
    web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .body()
        .unwrap()
        .last_child()
        .unwrap()
        .dyn_ref::<web_sys::Element>()
        .unwrap()
        .inner_html()
}

pub fn find_el(selector: &str) -> web_sys::HtmlElement {
    web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .query_selector(selector)
        .unwrap()
        .unwrap()
        .dyn_into()
        .unwrap()
}

#[derive(Debug, Clone, PartialEq, Default)]
pub struct CounterState(pub i32);

impl Cstate for CounterState {
    fn as_any(&self) -> &dyn std::any::Any {
        self
    }

    fn clone(&self) -> Box<dyn Cstate> {
        Box::new(std::clone::Clone::clone(self))
    }

    fn equals(&self, other: &dyn Cstate) -> bool {
        other
            .as_any()
            .downcast_ref::<Self>()
            .map(|o| o == self)
            .unwrap_or(false)
    }
}

pub fn test_counter_component(i: Option<u8>) -> Rc<RefCell<Component>> {
    let id = format!("counter_{}", i.unwrap_or_default());

    let view_fn = Box::new(move |state: &dyn Cstate| -> Vdom {
        let state = state.as_any().downcast_ref::<CounterState>().unwrap();
        Vdom::element("div", vec![], vec![Vdom::text(&format!("{}", state.0))])
    });
    Rc::new(RefCell::new(Component::new(
        &id,
        None,
        Box::<CounterState>::default(),
        HashMap::new(),
        view_fn,
    )))
}

mod common;
use common::*;

use wasm_bindgen_test::*;
wasm_bindgen_test_configure!(run_in_browser);
use std::{cell::RefCell, rc::Rc};
use toy_fe_fwk::*;
use web_sys::wasm_bindgen::JsCast;

#[wasm_bindgen_test]
fn mount_text() {
    let vdom = Vdom::text("hello");
    let component = test_component(vdom);
    mount_app(component);
    assert_eq!("hello", parent());
}

#[wasm_bindgen_test]
fn save_created_text_in_vdom() {
    let vdom = Vdom::text("hello");
    let component = test_component(vdom);
    mount_app(Rc::clone(&component));

    let mut component_rc = component.borrow_mut();
    let vdom = component_rc.vdom.as_mut().unwrap();
    assert!(vdom.node.as_ref().unwrap().has_type::<web_sys::Text>());
    assert_eq!("hello", vdom.node.as_ref().unwrap().text_content().unwrap());
}

#[wasm_bindgen_test]
fn mount_element() {
    let vdom = Vdom::element("div", vec![], vec![Vdom::text("hello")]);
    let component = test_component(vdom);
    mount_app(component);
    assert_eq!("<div>hello</div>", parent());
}

#[wasm_bindgen_test]
fn save_created_element_in_vdom() {
    let vdom = Vdom::element("div", vec![], vec![]);
    let component = test_component(vdom);
    mount_app(Rc::clone(&component));

    let mut component_rc = component.borrow_mut();
    let vdom = component_rc.vdom.as_mut().unwrap();
    assert!(vdom
        .node
        .as_ref()
        .unwrap()
        .has_type::<web_sys::HtmlDivElement>());
}

#[wasm_bindgen_test]
fn mount_fragment() {
    let vdom = Vdom::fragment(vec![Vdom::text("hello, "), Vdom::text("world")]);
    let component = test_component(vdom);
    mount_app(component);
    assert_eq!("hello, world", parent());
}

#[wasm_bindgen_test]
fn mount_fragment_inside_fragment() {
    let vdom = Vdom::fragment(vec![
        Vdom::element("p", vec![], vec![Vdom::text("foo")]),
        Vdom::fragment(vec![
            Vdom::element("p", vec![], vec![Vdom::text("bar")]),
            Vdom::element("p", vec![], vec![Vdom::text("baz")]),
        ]),
    ]);
    let component = test_component(vdom);
    mount_app(component);
    assert_eq!("<p>foo</p><p>bar</p><p>baz</p>", parent());
}

#[wasm_bindgen_test]
fn mount_fragment_with_children_and_attributes() {
    let vdom = Vdom::fragment(vec![
        Vdom::element(
            "span",
            vec![Vproperty::Id(Some("foo".to_string()))],
            vec![Vdom::text("hello, ")],
        ),
        Vdom::element(
            "span",
            vec![Vproperty::Id(Some("bar".to_string()))],
            vec![Vdom::text("world")],
        ),
    ]);
    let component = test_component(vdom);
    mount_app(component);
    assert_eq!(
        "<span id=\"foo\">hello, </span><span id=\"bar\">world</span>",
        parent()
    );
}

#[wasm_bindgen_test]
fn mount_element_with_id() {
    let vdom = Vdom::element("div", vec![Vproperty::Id(Some("foo".to_string()))], vec![]);
    let component = test_component(vdom);
    mount_app(component);
    assert_eq!("<div id=\"foo\"></div>", parent());
}

#[wasm_bindgen_test]
fn mount_element_with_class() {
    let vdom = Vdom::element(
        "div",
        vec![Vproperty::Class(Some("foo".to_string()))],
        vec![],
    );
    let component = test_component(vdom);
    mount_app(component);
    assert_eq!("<div class=\"foo\"></div>", parent());
}

#[wasm_bindgen_test]
fn mount_element_with_list_of_classes() {
    let vdom = Vdom::element(
        "div",
        vec![
            Vproperty::Class(Some("foo".to_string())),
            Vproperty::Class(Some("bar".to_string())),
        ],
        vec![],
    );
    let component = test_component(vdom);
    mount_app(component);
    assert_eq!("<div class=\"foo bar\"></div>", parent());
}

#[wasm_bindgen_test]
fn mount_element_with_styles() {
    let vdom = Vdom::element(
        "div",
        vec![Vproperty::Style((
            "color".to_string(),
            Some("red".to_string()),
        ))],
        vec![],
    );
    let component = test_component(vdom);
    mount_app(Rc::clone(&component));
    assert_eq!("<div style=\"color: red;\"></div>", parent());

    let mut component_rc = component.borrow_mut();
    let vdom = component_rc.vdom.as_mut().unwrap();
    assert_eq!(
        "red",
        vdom.node
            .as_ref()
            .unwrap()
            .dyn_ref::<web_sys::HtmlElement>()
            .unwrap()
            .style()
            .get_property_value("color")
            .unwrap()
    );
}

use wasm_bindgen_test::*;
wasm_bindgen_test_configure!(run_in_browser);
use toy_fe_fwk::*;

#[wasm_bindgen_test]
fn equal_property() {
    let old = Vproperty::Data(("key".to_string(), Some("val".to_string())));
    let new = Vproperty::Data(("key".to_string(), Some("val".to_string())));

    let diff = diff_props(&[&old], &[&new]);

    assert!(diff.added.is_empty());
    assert!(diff.removed.is_empty());
    assert!(diff.updated.is_empty());
}

#[wasm_bindgen_test]
fn add_property() {
    let new = Vproperty::Data(("key".to_string(), Some("val".to_string())));

    let diff = diff_props(&[], &[&new]);

    assert_eq!(
        diff.added,
        vec![Vproperty::Data((
            "key".to_string(),
            Some("val".to_string())
        ))]
    );
    assert!(diff.removed.is_empty());
    assert!(diff.updated.is_empty());
}

#[wasm_bindgen_test]
fn remove_property() {
    let old = Vproperty::Data(("key".to_string(), Some("val".to_string())));

    let diff = diff_props(&[&old], &[]);

    assert!(diff.added.is_empty());
    assert_eq!(
        diff.removed,
        vec![Vproperty::Data((
            "key".to_string(),
            Some("val".to_string())
        ))]
    );
    assert!(diff.updated.is_empty());
}

#[wasm_bindgen_test]
fn update_property() {
    let old = Vproperty::Data(("key".to_string(), Some("val_01".to_string())));
    let new = Vproperty::Data(("key".to_string(), Some("val_02".to_string())));

    let diff = diff_props(&[&old], &[&new]);
    web_sys::console::log_1(
        &format!(
            "added: {:?}, removed: {:?}, updated: {:?}",
            diff.added, diff.removed, diff.updated
        )
        .into(),
    );
    assert!(diff.added.is_empty());
    assert!(diff.removed.is_empty());
    assert_eq!(
        diff.updated,
        vec![Vproperty::Data((
            "key".to_string(),
            Some("val_02".to_string())
        ))]
    );
}

mod common;
use common::*;

use wasm_bindgen_test::*;
wasm_bindgen_test_configure!(run_in_browser);
use std::{cell::RefCell, rc::Rc};
use toy_fe_fwk::*;
use web_sys::wasm_bindgen::JsCast;

#[wasm_bindgen_test]
#[should_panic(expected = "Component is already mounted")]
fn can_not_be_mounted_twice() {
    let body = web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .body()
        .unwrap();
    let parent = create_element("div").unwrap();
    body.append_child(&parent).unwrap();

    let vdom = Vdom::text("hello");
    let component = test_component(vdom);
    Component::mount(&component, &parent, None);
    Component::mount(&component, &parent, None);
}

#[wasm_bindgen_test]
fn can_have_props() {
    let vdom = Vdom::element(
        "div",
        vec![Vproperty::Id(Some("foo".to_string()))],
        vec![Vdom::text("hello")],
    );
    let component = test_component(vdom);
    mount_app(component);
    assert_eq!("<div id=\"foo\">hello</div>", parent());
}

#[wasm_bindgen_test]
#[should_panic]
fn can_not_patch_dom_if_not_mounted() {
    let body = web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .body()
        .unwrap();
    let parent = create_element("div").unwrap();
    body.append_child(&parent).unwrap();

    let mut old = Vdom::text("hello");
    let component = test_component(old.clone());
    let mut new = Vdom::text("bar");

    patch_dom(&mut old, &mut new, &parent, &component);
}

#[wasm_bindgen_test]
fn when_state_changes_dom_is_patched() {
    let component = test_counter_component(None);
    mount_app(Rc::clone(&component));
    assert_eq!("<div>0</div>", parent());

    let new_state = Box::new(CounterState(1));
    Component::set_state(&component, new_state);
    assert_eq!("<div>1</div>", parent());
}

#[wasm_bindgen_test]
fn when_the_components_toplevel_element_is_not_fragment_it_has_no_offset() {
    let vdom = Vdom::element(
        "div",
        vec![],
        vec![
            Vdom::component(test_counter_component(Some(1))),
            Vdom::component(test_counter_component(Some(2))),
            Vdom::component(test_counter_component(Some(3))),
        ],
    );
    let component = test_component(vdom);
    mount_app(Rc::clone(&component));

    let component_rc = component.borrow();
    let vdom = component_rc.vdom.as_ref().unwrap();
    let children = vdom.extract_children();
    let first_offset = match &children[0].borrow().kind {
        VdomKind::Component(c) => c.borrow().offset(),
        _ => unreachable!(),
    };
    let second_offset = match &children[1].borrow().kind {
        VdomKind::Component(c) => c.borrow().offset(),
        _ => unreachable!(),
    };
    let third_offset = match &children[2].borrow().kind {
        VdomKind::Component(c) => c.borrow().offset(),
        _ => unreachable!(),
    };

    assert_eq!(0, first_offset);
    assert_eq!(0, second_offset);
    assert_eq!(0, third_offset);
}

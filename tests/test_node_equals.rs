mod common;
use common::*;

use wasm_bindgen_test::*;
wasm_bindgen_test_configure!(run_in_browser);
use toy_fe_fwk::*;

#[wasm_bindgen_test]
fn two_nodes_with_different_types_are_not_equal() {
    let node_one = Vdom::element("p", vec![], vec![Vdom::text("foo")]);
    let node_two = Vdom::text("foo");
    let node_three = Vdom::fragment(vec![node_one.clone(), node_two.clone()]);

    assert_ne!(node_one, node_two);
    assert_ne!(node_one, node_three);
    assert_ne!(node_two, node_three);
}

#[wasm_bindgen_test]
fn two_text_nodes_are_always_equal() {
    let node_one = Vdom::text("foo");
    let node_two = Vdom::text("bar");

    assert_eq!(node_one, node_two);
}

#[wasm_bindgen_test]
fn two_fragment_nodes_are_always_equal() {
    let node_one = Vdom::fragment(vec![Vdom::text("foo")]);
    let node_two = Vdom::fragment(vec![Vdom::text("bar")]);

    assert_eq!(node_one, node_two);
}

#[wasm_bindgen_test]
fn two_element_nodes_are_equal_if_they_have_the_same_tag() {
    let node_one = Vdom::element("p", vec![], vec![Vdom::text("foo")]);
    let node_two = Vdom::element("p", vec![], vec![Vdom::text("bar")]);
    let node_three = Vdom::element("div", vec![], vec![Vdom::text("foo")]);

    assert_eq!(node_one, node_two);
    assert_ne!(node_one, node_three);
}

#[wasm_bindgen_test]
fn two_components_are_equal_if_they_have_the_same_id() {
    let vdom1 = Vdom::element("p", vec![], vec![Vdom::text("foo")]);
    let component1 = test_component(vdom1);
    let vdom2 = Vdom::element("div", vec![], vec![Vdom::text("foo")]);
    let component2 = test_component(vdom2);
    assert_eq!(component1, component2);
}
